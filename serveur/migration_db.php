<?php
ini_set('display_errors', TRUE);
error_reporting(E_ALL);
	try
		{
		$bdd = new PDO('mysql:host=localhost; dbname=livres', 'root', 'mbg');
		}
	catch(Exception $e)
		{
		die('Erreur du système');
		}
	try
		{
		$bdd2 = new PDO('mysql:host=localhost; dbname=lecturaddicts', 'root', 'mbg');
		}
	catch(Exception $e)
		{
		die('Erreur du système');
		}
    echo "BDDs instanciées";
    
    /* membres */
	$req=$bdd->query('SELECT * FROM membres ORDER BY ID');
	while ($donnees=$req->fetch()) {
		$rep=$bdd2->prepare('INSERT INTO membres_membre(id, username, password, is_staff, is_superuser, is_active) VALUES(:id, :nom, :pwd, :admin, :admin2, 1)');
		$rep->execute(array('id' => $donnees['ID'], 'nom' => utf8_decode($donnees['Pseudo']), 'pwd' => 'sha1$'.$donnees['Mdp'], 'admin' => ($donnees['R'] == "Administrateur"), 'admin2' => ($donnees['R'] == "Administrateur")));
		$rep->closeCursor();
	}
	$req->closeCursor();
	echo 'fin des membres<br />';
    
    /* auteurs */
	$req=$bdd->query('SELECT * FROM auteurs ORDER BY ID');
	while ($donnees=$req->fetch()) {
		$rep=$bdd2->prepare('INSERT INTO livres_auteur(id, nom, prenom, membre_id, allowed) VALUES(:id, :nom, :prenom, 1, :allowed)');
		$rep->execute(array('id' => $donnees['ID'], 'nom' => utf8_decode($donnees['NA']), 'prenom' => utf8_decode($donnees['PA']), 'allowed' => $donnees['OK']));
		$rep->closeCursor();
	}
	$req->closeCursor();
	echo 'fin des auteurs<br />';
	
	/* editeurs */
	$req=$bdd->query('SELECT * FROM editions ORDER BY ID');
	while ($donnees=$req->fetch()) {
	    echo $donnees['Ed'].'<br />';
		$rep=$bdd2->prepare('INSERT INTO livres_editeur(id, nom, membre_id, allowed) VALUES(:id, :nom, 1, :allowed)');
		$rep->execute(array('id' => $donnees['ID'], 'nom' => utf8_decode($donnees['Ed']), 'allowed' => $donnees['OK']));
		$rep->closeCursor();
	}
	$req->closeCursor();
	echo 'fin des editeurs<br />';
    
    /* livres */
	$req=$bdd->query('SELECT * FROM livres ORDER BY ID');
	while ($donnees=$req->fetch()) {
	    /* livre */
		$rep=$bdd2->prepare('INSERT INTO livres_livre(id, titre_livre, titre_serie, allowed, date_modification, numero_tome, membre_id) 
		                    VALUES(:id, :titre_livre, :titre_serie, :allowed, :date_modification, :numero_tome, 1)');
		if($donnees['S'] != ' ') {
		    $s = $donnees['S'];
		    $s = explode(' (T', $s);
		    $t = trim(explode(')', $s[1])[0]);
		    $s = $s[0];
		}
		else {
		    $s = null;
		    $t = null;
		}
		$rep->execute(array('id'=> $donnees['ID'],
		                    'titre_livre' => utf8_decode($donnees['L']),
		                    'titre_serie' => utf8_decode($s), 
		                    'allowed' => $donnees['OK'], 
		                    'date_modification' => $donnees['Dm'],
		                    'numero_tome' => $t));
		$rep->closeCursor();

		/* auteur */
		$rep=$bdd2->prepare('INSERT INTO livres_livre_auteurs(livre_id, auteur_id) VALUES(:livre_id, :auteur_id)');
		$rep->execute(array('livre_id' => $donnees['ID'], 'auteur_id' => $donnees['A']));
		$rep->closeCursor();

		/* couverture */
		$rep=$bdd2->prepare('INSERT INTO livres_couverture(livre_id, editeur_id, couverture, date_publication, membre_id, allowed) VALUES(:livre_id, :editeur_id, :couverture, :date, 1, 1)');
		$rep->execute(array('livre_id' => $donnees['ID'], 'editeur_id' => $donnees['E'], 'couverture' => 'couvertures/'.$donnees['ID'], 'date' => $donnees['D']));
		$rep->closeCursor();

		/* resume */
		$rep=$bdd2->prepare('INSERT INTO membres_resume(livre_id, valeur, membre_id, allowed) VALUES(:livre_id, :valeur, 1, 1)');
		$rep->execute(array('livre_id' => $donnees['ID'], 'valeur' => utf8_decode($donnees['R'])));
		$rep->closeCursor();
	}
	echo 'fin des livres<br />';
	
	/* commentaires */
	$req=$bdd->query('SELECT * FROM commentaires ORDER BY ID');
	while ($donnees=$req->fetch()) {
		$rep=$bdd2->prepare('INSERT INTO membres_commentaire(valeur, livre_id, membre_id, date) VALUES(:valeur, :livre_id, :membre_id, :date)');
		$rep->execute(array('valeur' => utf8_decode($donnees['C']), 'livre_id' => $donnees['ID_fiche'], 'membre_id' => $donnees['ID_membre'], 'date' => $donnees['D']));
		$rep->closeCursor();
	}
	$req->closeCursor();
	echo 'fin des commentaires<br />';
	
	/* messages */
	$req=$bdd->query('SELECT * FROM `messages` ORDER BY ID');
	while ($donnees=$req->fetch()) {
		$rep=$bdd2->prepare('INSERT INTO membres_message(valeur, sujet, membre_id, date, membre_destinataire_id, lu) VALUES(:valeur, :sujet, :membre_id, :date, :membre_id2, :lu)');
		$rep->execute(array('valeur' => utf8_decode($donnees['C']), 'membre_id2' => $donnees['ID_d'], 'membre_id' => $donnees['ID_e'], 'date' => $donnees['D'], 'sujet' => utf8_decode($donnees['S']), 'lu' => $donnees['L']));
		$rep->closeCursor();
	}
	$req->closeCursor();
	echo 'fin des messages<br />';
	
	/* ages */
	$req=$bdd->query('SELECT * FROM age ORDER BY ID');
	while ($donnees=$req->fetch()) {
		$rep=$bdd2->prepare('INSERT INTO membres_age(value, livre_id, membre_id) VALUES(:valeur, :livre_id, :membre_id)');
		$rep->execute(array('valeur' => $donnees['Age'], 'livre_id' => $donnees['ID_fiche'], 'membre_id' => $donnees['ID_membre']));
		$rep->closeCursor();
	}
	$req->closeCursor();
	echo 'fin des ages<br />';
	
	/* notes */
	$req=$bdd->query('SELECT * FROM notes ORDER BY ID');
	while ($donnees=$req->fetch()) {
		$rep=$bdd2->prepare('INSERT INTO membres_note(valeur, livre_id, membre_id) VALUES(:valeur, :livre_id, :membre_id)');
		$rep->execute(array('valeur' => $donnees['N'], 'livre_id' => $donnees['ID_fiche'], 'membre_id' => $donnees['ID_membre']));
		$rep->closeCursor();
	}
	$req->closeCursor();
	echo 'fin des notes<br />';
	
	/* thèmes */
	/* proposés */
	$themes=array(
		'Am'=>array('i'=>'Amour','d'=>'Une histoire d\'amour est la composante essentielle, mais pas forcément principale, de l\'histoire'),
		'An'=>array('i'=>'Anges','d'=>'Les personnages principaux sont des anges ou leurs descendants, des néphilims'),
		'V'=>array('i'=>'Vampires','d'=>'Les personnages principaux sont des vampires'),
		'MMA'=>array('i'=>'Monde moyen-âgeux','d'=>'Présence de chevaliers, de châteaux-forts, ... et/ou absence de technologies modernes <br />
							Note : un monde moyen-âgeux peut aussi être post-apocalyptique...'),
		'MPA'=>array('i'=>'Monde post-apocalyptique','d'=>'L\'histoire se déroule après une apocalypse : pandémie mondiale, guerre nucléaire, explosion de la Terre...'),
		'LG'=>array('i'=>'Loups-garous','d'=>'Les personnages principaux sont des loups-garous'),
		'I'=>array('i'=>'Immortalité','d'=>'Les personnages principaux sont immortels'),
		'M'=>array('i'=>'Magie','d'=>'Présence de magie'),
		'MP'=>array('i'=>'Mondes parallèles','d'=>'Coexistence de plusieurs mondes, avec passage des uns aux autres'),
		'E'=>array('i'=>'Espionnage','d'=>'Les personnages principaux sont des espions, ou l\'espionage prend une place importante dans l\'oeuvre'),
		'Re'=>array('i'=>'Révolte','d'=>'Insurrection contre le système (ou gouvernement) en place, qui est très généralement totalitaire et/ou oppressif'),
		'Po'=>array('i'=>'Policier','d'=>'Enquête policière'),
		'Ic'=>array('i'=>'Inclassable','d'=>'Oeuvre qui sort de tout cadre, déroutante voire même absurde au début ou en première lecture...<br />
							Note : A lire jusqu\'à la fin pour connaitre la "raison" de l\'"absurdité"...'),
		'Fu'=>array('i'=>'Futuriste','d'=>'Monde très avancé technologiquement, science-fiction'),
		'H'=>array('i'=>'Humour','d'=>''),
		'T'=>array('i'=>'Pièce de théâtre','d'=>''),
		'B'=>array('i'=>'Biographie','d'=>''),
		'Ph'=>array('i'=>'Essai philosophique','d'=>''),
		'Ge'=>array('i'=>'Recueil de poèmes','d'=>''),
		'C'=>array('i'=>'Classique', 'd'=>'Classique de la littérature'),
		'Cf'=>array('i'=>'Créatures fantastiques', 'd'=>''),
		'Ta'=>array('i'=>'Triangle amoureux', 'd'=>''),
		'Ce'=>array('i'=>'Célébrité', 'd'=>''),
		'Dy'=>array('i'=>'Dystopie', 'd'=>'Contre-utopie'),
		'Cs'=>array('i'=>'Engagé', 'd'=>'Oeuvre qui dénonce des dérives de notre société'),
		'Ep'=>array('i'=>'Epopée', 'd'=>'')
		);
	foreach($themes as $element) {
	    $rep=$bdd2->prepare('INSERT INTO livres_themepropose(nom, description, membre_id, allowed) VALUES(:nom, :description, 1, 1)');
		$rep->execute(array('nom' => utf8_decode($element['i']), 'description' => utf8_decode($element['d'])));
		$rep->closeCursor();
	}
	
	/* votés */
	function trouve_abbreviation_theme($nom, $tab) {
	    foreach($tab as $cle=>$element) {
	        if($element['i'] == utf8_decode($nom)) {
	            return $cle;
	        }
	    }
	};
	
	$rep=$bdd2->query('SELECT * FROM livres_themepropose ORDER BY id');
	$theme_reverse = array();
	while($donnees = $rep->fetch()) {
	    $theme_reverse[trouve_abbreviation_theme($donnees['nom'], $themes)] = $donnees['id'];
	    echo trouve_abbreviation_theme($donnees['nom'], $themes).' => '.$donnees['id'].'<br />';
	}
	$rep->closeCursor();
	
	$req=$bdd->query('SELECT * FROM genres ORDER BY ID');
	while ($donnees=$req->fetch()) {
		$rep=$bdd2->prepare('INSERT INTO membres_theme(valeur_id, livre_id, membre_id) VALUES(:valeur_id, :livre_id, :membre_id)');
		$rep->execute(array('valeur_id' => $theme_reverse[$donnees['G']], 'livre_id' => $donnees['ID_fiche'], 'membre_id' => $donnees['ID_membre']));
		$rep->closeCursor();
	}
	$req->closeCursor();
	
	echo 'fin des themes<br />';
	
	
	echo 'fin de la migration<br />';
	
?>
