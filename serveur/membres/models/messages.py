# -*- coding: utf-8 -*-
from django.db import models
from membres import Membre
from rest_framework import viewsets, serializers, permissions
from lecturaddicts.perms import IsOwnerOrReadOnly, IsDestinationOrNone

class Message(models.Model):
    membre_destinataire = models.ForeignKey(Membre, related_name='membre_destinataire')
    membre = models.ForeignKey(Membre, related_name='membre_emetteur')
    sujet = models.CharField(blank=True, max_length=100)
    valeur = models.TextField()
    date = models.DateTimeField(auto_now=True)
    lu = models.BooleanField(default=False)
    
    class Meta:
        app_label = 'membres'

class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        
    membre_destinataire = serializers.PrimaryKeyRelatedField(queryset=Membre.objects.all())
    membre = serializers.PrimaryKeyRelatedField(queryset=Membre.objects.all())

    def create(self, validated_data):
        o = Message.objects.create(**validated_data)
        o.membre = self.context.get('request').user
        return o

class MessageViewSet(viewsets.ModelViewSet):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer
    permission_classes = (IsOwnerOrReadOnly, IsDestinationOrNone)

    def create(self, request):
        s = MessageSerializer(data=request.data, context={'request': request})
        if s.is_valid():
            s.save()
            return Response(s.data, status=status.HTTP_201_CREATED)
        else:
            return Response(s.errors, status=status.HTTP_400_BAD_REQUEST)
