# -*- coding: utf-8 -*-
from django.db import models
from membres import Membre
from django.db.models import Count
from livres.models import Livre
from rest_framework import viewsets, serializers, permissions, decorators
from rest_framework.response import Response
from lecturaddicts.perms import IsOwnerOrReadOnly

class Theme(models.Model):
    membre = models.ForeignKey(Membre)
    livre = models.ForeignKey('livres.Livre', related_name="themes")
    valeur = models.ForeignKey('livres.ThemePropose', related_name='theme_id')
    
    class Meta:
        app_label = 'membres'

class ThemeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Theme
        
    membre = serializers.PrimaryKeyRelatedField(queryset=Membre.objects.all())
    livre = serializers.PrimaryKeyRelatedField(queryset=Livre.objects.all())
    valeur = serializers.PrimaryKeyRelatedField(read_only=True)

    def create(self, validated_data):
        o = Theme.objects.create(**validated_data)
        o.membre = self.context.get('request').user
        return o

class ThemeViewSet(viewsets.ModelViewSet):
    queryset = Theme.objects.all()
    serializer_class = ThemeSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly,)

    def create(self, request):
        s = ThemeSerializer(data=request.data, context={'request': request})
        if s.is_valid():
            s.save()
            return Response(s.data, status=status.HTTP_201_CREATED)
        else:
            return Response(s.errors, status=status.HTTP_400_BAD_REQUEST)
