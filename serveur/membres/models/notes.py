# -*- coding: utf-8 -*-
from django.db import models
from membres import Membre
from livres.models import Livre
from django.db.models import Avg, Count
from rest_framework import viewsets, serializers, permissions, decorators
from rest_framework.response import Response
from lecturaddicts.perms import IsOwnerOrReadOnly
from lecturaddicts.fields import DecimalRangeField

class Note(models.Model):
    membre = models.ForeignKey(Membre)
    livre = models.ForeignKey('livres.Livre', related_name='note')
    valeur = DecimalRangeField(max_digits=4, decimal_places=1, min_value=0.0, max_value=10.0, step=0.5)
    
    class Meta:
        app_label = 'membres'

class NoteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Note
        
    membre = serializers.PrimaryKeyRelatedField(queryset=Membre.objects.all())
    livre = serializers.PrimaryKeyRelatedField(queryset=Livre.objects.all())

    def create(self, validated_data):
        o = Note.objects.create(**validated_data)
        o.membre = self.context.get('request').user
        return o

class NoteViewSet(viewsets.ModelViewSet):
    queryset = Note.objects.all()
    serializer_class = NoteSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly, )

    def create(self, request):
        livre = Livre.objects.get(pk=request.data.get("livre"))
        valeur = request.data.get("valeur")
        membre = request.user
        try:
            note = Note.objects.get(livre=livre, membre=membre)
            note.valeur = valeur
        except Note.DoesNotExist:
            note = Note(livre=livre, membre=membre, valeur=valeur)

        note.save()
        s = NoteSerializer(note)
        #serializer = self.get_serializer(data=request.data)
        #serializer.is_valid(raise_exception=True)

        return Response(s.data, 201)

    @decorators.list_route(methods=['get'])
    def livre(self, request):
        livre = request.GET.get("livre")
        notes = Note.objects.filter(livre=livre).aggregate(Count('id'), Avg('valeur'))

        print notes
        #serializer = NoteSerializer(notes, many=True)
        return Response(notes, 200) #serializer.data, 200)

    @decorators.list_route(methods=['get'])
    def livre_membre(self, request):
        livre = Livre.objects.get(pk=request.GET.get("livre"))
        print request.user
        try:
            notes = Note.objects.get(livre=livre, membre=request.user)
        except Note.DoesNotExist:
            notes = Note(livre=livre, membre=request.user, valeur=0)
        
        print notes
        serializer = NoteSerializer(notes)
        return Response(serializer.data, 200)
