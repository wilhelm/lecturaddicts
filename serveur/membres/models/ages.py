# -*- coding: utf-8 -*-
from django.db import models
from membres import Membre
from django.db.models import Count
from rest_framework import viewsets, serializers, permissions, decorators
from rest_framework.response import Response
from lecturaddicts.perms import IsOwnerOrReadOnly

class Age(models.Model):
    membre = models.ForeignKey(Membre)
    livre = models.ForeignKey('livres.Livre', related_name='age')
    Tous_publics = 1
    A8_10 = 2
    A10_12 = 3
    A12_14 = 4
    A14_16 = 5
    Young_adults = 6
    Adults = 7
    AGES = (
        (Tous_publics,'Tous publics'),
	    (A8_10, '8-10 ans'),
	    (A10_12, '10-12 ans'),
	    (A12_14, '12-14 ans'),
	    (A14_16, '14-16 ans'),
	    (Young_adults, 'Jeunes Adultes'),
	    (Adults, 'Adultes')
    )
    value = models.CharField(max_length=2,
                             choices=AGES)
    
    class Meta:
        app_label = 'membres'

class AgeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Age
        
    membre = serializers.PrimaryKeyRelatedField(read_only=True)
    livre = serializers.PrimaryKeyRelatedField(read_only=True)

    def create(self, validated_data):
        o = Age.objects.create(**validated_data)
        o.membre = self.context.get('request').user
        return o

class AgeViewSet(viewsets.ModelViewSet):
    queryset = Age.objects.all()
    serializer_class = AgeSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly,)

    def create(self, request):
        s = AgeSerializer(data=request.data, context={'request': request})
        if s.is_valid():
            s.save()
            return Response(s.data, status=status.HTTP_201_CREATED)
        else:
            return Response(s.errors, status=status.HTTP_400_BAD_REQUEST)


    @decorators.list_route(methods=['get'])
    def livre(self, request):
        print request.GET.get("livre")
        ages = Age.objects.filter(livre=request.GET.get("livre")).annotate(m=Count('id')).values('value', 'm')
        print ages

        liste = []
        for age in Age.AGES:
            b = False
            for donnees in ages:
                if(int(donnees['value']) == age[0]):
                    print "ok"
                    donnees.update({'nom': age[1]})
                    liste.append(donnees)
                    b = True
            if(not b):
                liste.append({'nom': age[1], "m": 0, 'value': age[0]})
        liste = sorted(liste, key=lambda age: age['m'], reverse=True)
        print liste
        #serializer = AgeSerializer(ages, many=True)
        return Response(liste, 200) #serializer.data, 200)
