# -*- coding: utf-8 -*-
from django.db import models
from rest_framework import viewsets, serializers, decorators, permissions
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser, _user_has_module_perms, _user_has_perm
from rest_framework.response import Response

class MembreManager(BaseUserManager):
    def create_user(self, username, password):
        membre = self.model(username=username)
        membre.set_password(password)
        membre.save(using=self._db)
        return membre

    def create_superuser(self, username, password):
        membre = self.create_user(username, password)
        membre.is_superuser = True
        membre.is_staff = True
        membre.save(using=self._db)
        return membre

class Membre(AbstractBaseUser):
    username = models.CharField(max_length=128, unique=True)
    firstname = models.CharField(max_length=128, blank=True)
    lastname = models.CharField(max_length=128, blank=True)
    email = models.EmailField(max_length=254, blank=True)

    is_active = models.BooleanField(default=True)
    last_modified = models.DateTimeField(auto_now=True)

    is_superuser = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)

    previous_login = models.DateTimeField(blank=True, auto_now_add=True)
    current_login = models.DateTimeField(blank=True, auto_now_add=True)

    objects = MembreManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = []

    def has_perm(self, perm, obj=None):
        if self.is_active and self.is_superuser:
            return True

        return _user_has_perm(self, perm, obj)

    def has_perms(self, perm_list, obj=None):
        for perm in perm_list:
            if not self.has_perm(perm, obj):
                return False
        return True

    def has_module_perms(self, app_label):
        if self.is_active and self.is_superuser:
            return True

        return _user_has_module_perms(self, app_label)

    def get_short_name(self):
        return self.username

    def get_full_name(self):
        return "%s %s" % (self.firstname, self.lastname)
    
    class Meta:
        app_label = 'membres'

class MembreSerializer(serializers.ModelSerializer):
    class Meta:
        model = Membre
        read_only_fields = ('is_active', 'last_login', 'last_modified', 'previous_login', 'is_staff', 'is_superuser')
        exclude = ('current_login', )
        extra_kwargs = {'password': { 'write_only': True, 'required':False }}

    def create(self, data):
        u = super(MembreSerializer, self).create(data)
        u.set_password(data.get('password', '0000'))
        u.save()
        return u

class MembreViewSet(viewsets.ModelViewSet):
    queryset = Membre.objects.all()
    serializer_class = MembreSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    @decorators.permission_classes((permissions.IsAuthenticated, ))
    @decorators.list_route(methods=['get'])
    def me(self, request):
        serializer = self.serializer_class(request.user)
        return Response(serializer.data)

    @decorators.permission_classes((permissions.IsAuthenticated, ))
    @decorators.list_route(methods=['put'])
    def change_password(self, request):
        membre = request.user

        data = request.data
        if not membre.check_password(data['old_password']):
            raise exceptions.PermissionDenied()

        if data['password'] == "":
            return Response("'password' field cannot be empty", 400)

        membre.set_password(data['password'])
        membre.save()
        return Response('Password changed', 200)
