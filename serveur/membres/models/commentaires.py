# -*- coding: utf-8 -*-
from django.db import models
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from membres import Membre, MembreSerializer
from livres.models import Livre
from rest_framework import viewsets, serializers, permissions, decorators
from rest_framework.response import Response
from lecturaddicts.perms import IsOwnerOrReadOnly
import locale

class Commentaire(models.Model):
    membre = models.ForeignKey(Membre)
    livre = models.ForeignKey('livres.Livre', related_name='commentaire')
    valeur = models.TextField()
    date = models.DateTimeField(auto_now=True)
    
    class Meta:
        app_label = 'membres'

class CommentaireSerializer(serializers.ModelSerializer):
    class Meta:
        model = Commentaire
        
    membre = serializers.PrimaryKeyRelatedField(queryset=Membre.objects.all())
    livre = serializers.PrimaryKeyRelatedField(queryset=Livre.objects.all())

    def create(self, validated_data):
        o = Comentaire.objects.create(**validated_data)
        o.membre = self.context.get('request').user
        return o

class CommentaireViewSet(viewsets.ModelViewSet):
    queryset = Commentaire.objects.all()
    serializer_class = CommentaireSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly, )

    def create(self, request):
        print request.data
        livre = Livre.objects.get(pk=request.data.get("livre"))
        valeur = request.data.get("valeur")
        membre = request.user
        commentaire = Commentaire(livre=livre, membre=membre, valeur=valeur)

        commentaire.save()
        s = CommentaireSerializer(commentaire)
        #serializer = self.get_serializer(data=request.data)
        #serializer.is_valid(raise_exception=True)

        return Response(s.data, 201)

    @decorators.list_route(methods=['get'])
    def livre(self, request):
        #print request.GET.get("livre")
        commentaires = Commentaire.objects.filter(livre=request.GET.get("livre")).order_by('-date')
        paginator = Paginator(commentaires, 10)
        page = request.GET.get('page')
        try:
            commentaires = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            commentaires = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            commentaires = paginator.page(paginator.num_pages)
        
        serializer = CommentaireSerializer(commentaires, many=True)
        return Response({'results': serializer.data, 'count': paginator.num_pages}, 200)
