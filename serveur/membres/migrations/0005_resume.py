# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('livres', '0005_livre_numero_tome'),
        ('membres', '0004_auto_20160305_2018'),
    ]

    operations = [
        migrations.CreateModel(
            name='Resume',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('valeur', models.TextField()),
                ('allowed', models.BooleanField(default=False)),
                ('livre', models.ForeignKey(to='livres.Livre')),
                ('membre', models.ForeignKey(related_name='membre_resume', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
