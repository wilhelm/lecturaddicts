# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('membres', '0006_auto_20160306_0916'),
    ]

    operations = [
        migrations.AddField(
            model_name='message',
            name='lu',
            field=models.BooleanField(default=False),
        ),
    ]
