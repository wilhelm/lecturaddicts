# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('membres', '0005_resume'),
    ]

    operations = [
        migrations.AddField(
            model_name='commentaire',
            name='date',
            field=models.DateTimeField(default=None, auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='message',
            name='date',
            field=models.DateTimeField(default=None, auto_now=True),
            preserve_default=False,
        ),
    ]
