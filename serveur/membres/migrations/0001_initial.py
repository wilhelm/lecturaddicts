# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Membre',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('pseudo', models.CharField(max_length=100)),
                ('password', models.CharField(max_length=100)),
                ('mail', models.EmailField(max_length=100, null=True, blank=True)),
                ('date_derniere_connexion', models.DateTimeField(auto_now=True)),
                ('is_admin', models.BooleanField(default=False)),
            ],
        ),
    ]
