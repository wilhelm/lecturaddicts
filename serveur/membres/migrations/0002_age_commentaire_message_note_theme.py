# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('livres', '0002_auto_20160305_1443'),
        ('membres', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Age',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.CharField(max_length=2)),
                ('livre', models.ForeignKey(to='livres.Livre')),
                ('membre', models.ForeignKey(to='membres.Membre')),
            ],
        ),
        migrations.CreateModel(
            name='Commentaire',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('valeur', models.TextField()),
                ('livre', models.ForeignKey(to='livres.Livre')),
                ('membre', models.ForeignKey(to='membres.Membre')),
            ],
        ),
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('sujet', models.CharField(max_length=100, blank=True)),
                ('valeur', models.TextField()),
                ('membre_destinataire', models.ForeignKey(related_name='membre_destinataire', to='membres.Membre')),
                ('membre_emetteur', models.ForeignKey(related_name='membre_emetteur', to='membres.Membre')),
            ],
        ),
        migrations.CreateModel(
            name='Note',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('valeur', models.FloatField()),
                ('livre', models.ForeignKey(to='livres.Livre')),
                ('membre', models.ForeignKey(to='membres.Membre')),
            ],
        ),
        migrations.CreateModel(
            name='Theme',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('livre', models.ForeignKey(to='livres.Livre')),
                ('membre', models.ForeignKey(to='membres.Membre')),
                ('valeur', models.ForeignKey(to='livres.ThemePropose')),
            ],
        ),
    ]
