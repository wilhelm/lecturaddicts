# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('membres', '0003_auto_20160305_1750'),
    ]

    operations = [
        migrations.RenameField(
            model_name='message',
            old_name='membre_emetteur',
            new_name='membre',
        ),
    ]
