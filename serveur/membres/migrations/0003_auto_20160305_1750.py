# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('membres', '0002_age_commentaire_message_note_theme'),
    ]

    operations = [
        migrations.RenameField(
            model_name='membre',
            old_name='is_admin',
            new_name='is_staff',
        ),
        migrations.RemoveField(
            model_name='membre',
            name='date_derniere_connexion',
        ),
        migrations.RemoveField(
            model_name='membre',
            name='mail',
        ),
        migrations.RemoveField(
            model_name='membre',
            name='pseudo',
        ),
        migrations.AddField(
            model_name='membre',
            name='current_login',
            field=models.DateTimeField(default=None, auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='membre',
            name='email',
            field=models.EmailField(max_length=254, blank=True),
        ),
        migrations.AddField(
            model_name='membre',
            name='firstname',
            field=models.CharField(max_length=128, blank=True),
        ),
        migrations.AddField(
            model_name='membre',
            name='is_active',
            field=models.BooleanField(default=True),
        ),
        migrations.AddField(
            model_name='membre',
            name='is_superuser',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='membre',
            name='last_login',
            field=models.DateTimeField(null=True, verbose_name='last login', blank=True),
        ),
        migrations.AddField(
            model_name='membre',
            name='last_modified',
            field=models.DateTimeField(default=None, auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='membre',
            name='lastname',
            field=models.CharField(max_length=128, blank=True),
        ),
        migrations.AddField(
            model_name='membre',
            name='previous_login',
            field=models.DateTimeField(default=None, auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='membre',
            name='username',
            field=models.CharField(default=None, unique=True, max_length=128),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='membre',
            name='password',
            field=models.CharField(max_length=128, verbose_name='password'),
        ),
    ]
