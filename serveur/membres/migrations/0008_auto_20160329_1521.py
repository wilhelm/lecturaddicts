# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('membres', '0007_message_lu'),
    ]

    operations = [
        migrations.AlterField(
            model_name='age',
            name='livre',
            field=models.ForeignKey(related_name='age', to='livres.Livre'),
        ),
        migrations.AlterField(
            model_name='age',
            name='value',
            field=models.CharField(max_length=2, choices=[(1, b'Tous publics'), (2, b'8-10 ans'), (3, b'10-12 ans'), (4, b'12-14 ans'), (5, b'14-16 ans'), (6, b'Jeunes Adultes'), (7, b'Adultes')]),
        ),
        migrations.AlterField(
            model_name='commentaire',
            name='livre',
            field=models.ForeignKey(related_name='commentaire', to='livres.Livre'),
        ),
        migrations.AlterField(
            model_name='note',
            name='livre',
            field=models.ForeignKey(related_name='note', to='livres.Livre'),
        ),
        migrations.AlterField(
            model_name='resume',
            name='livre',
            field=models.ForeignKey(related_name='resumes', to='livres.Livre'),
        ),
        migrations.AlterField(
            model_name='theme',
            name='livre',
            field=models.ForeignKey(related_name='themes', to='livres.Livre'),
        ),
        migrations.AlterField(
            model_name='theme',
            name='valeur',
            field=models.ForeignKey(related_name='theme_id', to='livres.ThemePropose'),
        ),
    ]
