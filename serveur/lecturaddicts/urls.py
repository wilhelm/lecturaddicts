"""lecturaddicts URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.contrib import admin
from django.views.generic import TemplateView

from django.conf.urls import patterns, include, url
from rest_framework import routers

from django.contrib import admin
admin.autodiscover()

#import permission
#permission.autodiscover()

from livres.models import AuteurViewSet, LivreViewSet, EditeurViewSet, CouvertureViewSet, ThemeProposeViewSet, ResumeViewSet
from membres.models import MembreViewSet, NoteViewSet, ThemeViewSet as Theme2ViewSet, AgeViewSet, MessageViewSet, CommentaireViewSet
#from bars_core.models.bar import BarViewSet, BarSettingsViewSet
#from bars_core.models.user import UserViewSet, ResetPasswordView
#from bars_core.models.role import RoleViewSet

#from bars_transactions.views import TransactionViewSet

router = routers.DefaultRouter()

router.register('auteurs', AuteurViewSet)
router.register('livres', LivreViewSet)
router.register('editeurs', EditeurViewSet)
router.register('couvertures', CouvertureViewSet)
router.register('resumes', ResumeViewSet)
router.register('themes_proposes', ThemeProposeViewSet, base_name="themes_proposes")

router.register('membres', MembreViewSet)
router.register('notes', NoteViewSet)
router.register('ages', AgeViewSet)
router.register('themes', Theme2ViewSet)
router.register('messages', MessageViewSet)
router.register('commentaires', CommentaireViewSet)
#router.register('bar', BarViewSet)
#router.register('barsettings', BarSettingsViewSet)
#router.register('user', UserViewSet)
#router.register('role', RoleViewSet)

#router.register('transaction', TransactionViewSet)

urlpatterns = [
    #url(r'^admin/', include(admin.site.urls)),
    #url(r'^accueil$', TemplateView.as_view(template_name='accueil.html'), name='accueil'),
    #url(r'^', include('livres.urls')),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^api-token-auth/', 'rest_framework_jwt.views.obtain_jwt_token'),
    #url(r'^api-token-auth/', 'bars_core.auth.obtain_jwt_token'),
    #url(r'^reset-password/$', ResetPasswordView.as_view()),
    url(r'^', include(router.urls)),
]
