# -*- coding: utf-8 -*-
import hashlib
from django.contrib.auth.hashers import BasePasswordHasher

class OldPasswordHasher(BasePasswordHasher):
    algorithm = 'sha1'
    
    def encode(password, salt):
        return hashlib.sha1(password).hexdigest()

    def safe_summary(encoded):
        return None

    def harden_runtime(self, password, encoded):
        pass

    def verify(self, password, encoded):
        return hashlib.sha1(password).hexdigest() == encoded.split('$')[1]
