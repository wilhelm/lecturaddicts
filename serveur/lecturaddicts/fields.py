from django.db import models

class DecimalRangeField(models.DecimalField):
    def __init__(self, verbose_name=None, name=None, min_value=None, max_value=None, max_digits=2, decimal_places=0, step=1, **kwargs):
        self.min_value, self.max_value, self.step = min_value, max_value, step
        models.DecimalField.__init__(self, verbose_name, name, max_digits, decimal_places, **kwargs)
    def formfield(self, **kwargs):
        defaults = {'min_value': self.min_value, 'max_value':self.max_value, 'step':self.step}
        defaults.update(kwargs)
        return super(DecimalRangeField, self).formfield(**defaults)
