# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('livres', '0005_livre_numero_tome'),
    ]

    operations = [
        migrations.AlterField(
            model_name='couverture',
            name='livre',
            field=models.ForeignKey(related_name='couvertures', to='livres.Livre'),
        ),
    ]
