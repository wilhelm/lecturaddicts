# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('livres', '0002_auto_20160305_1443'),
    ]

    operations = [
        migrations.AddField(
            model_name='auteur',
            name='membre',
            field=models.ForeignKey(related_name='membre_auteur', default=1, to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='couverture',
            name='membre',
            field=models.ForeignKey(related_name='membre_couverture', default=1, to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='editeur',
            name='membre',
            field=models.ForeignKey(related_name='membre_editeur', default=1, to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='livre',
            name='membre',
            field=models.ForeignKey(related_name='membre_livre', default=1, to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='themepropose',
            name='membre',
            field=models.ForeignKey(related_name='membre_theme_propose', to=settings.AUTH_USER_MODEL),
        ),
    ]
