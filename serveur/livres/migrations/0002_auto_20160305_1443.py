# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('membres', '0001_initial'),
        ('livres', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ThemePropose',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nom', models.CharField(max_length=25)),
                ('description', models.CharField(max_length=100)),
                ('allowed', models.BooleanField(default=False)),
                ('membre', models.ForeignKey(related_name='membre_id', to='membres.Membre')),
            ],
        ),
        migrations.RenameField(
            model_name='livre',
            old_name='auteur',
            new_name='auteurs',
        ),
    ]
