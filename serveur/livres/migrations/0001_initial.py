# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Auteur',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('prenom', models.CharField(max_length=50, null=True, blank=True)),
                ('nom', models.CharField(max_length=50)),
                ('allowed', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='Couverture',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_publication', models.DateField(default=datetime.date(1970, 1, 1))),
                ('allowed', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='Editeur',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nom', models.CharField(max_length=50)),
                ('allowed', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='Livre',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('titre_livre', models.CharField(max_length=100)),
                ('titre_serie', models.CharField(max_length=100, null=True, blank=True)),
                ('allowed', models.BooleanField(default=False)),
                ('date_modification', models.DateTimeField(auto_now=True)),
                ('auteur', models.ManyToManyField(to='livres.Auteur')),
            ],
        ),
        migrations.AddField(
            model_name='couverture',
            name='editeur',
            field=models.ForeignKey(to='livres.Editeur'),
        ),
        migrations.AddField(
            model_name='couverture',
            name='livre',
            field=models.ForeignKey(to='livres.Livre'),
        ),
    ]
