# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('livres', '0004_auto_20160306_0900'),
    ]

    operations = [
        migrations.AddField(
            model_name='livre',
            name='numero_tome',
            field=models.FloatField(null=True, blank=True),
        ),
    ]
