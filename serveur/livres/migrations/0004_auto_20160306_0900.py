# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('livres', '0003_auto_20160305_2018'),
    ]

    operations = [
        migrations.AddField(
            model_name='couverture',
            name='couverture',
            field=models.ImageField(default=None, upload_to=b'couvertures/'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='couverture',
            name='miniature',
            field=models.ImageField(default=None, upload_to=b'miniatures/'),
            preserve_default=False,
        ),
    ]
