#-*-coding: utf-8 -*-
from django.shortcuts import render
from livres.models import Livre
from commun.views import compteur

def par_titre(request):
    compte = compteur()
    titres = Livre.objects.raw('SELECT id, SUBSTR(IF(NOT(titre_serie=NULL), titre_serie, titre_livre), 1, 1) as pl  FROM `livres_livre` WHERE allowed=1 GROUP BY SUBSTR(IF(NOT(titre_serie=NULL), titre_serie, titre_livre), 1, 1) ')
    return render(request, 'livres/par_titre.html', locals())
