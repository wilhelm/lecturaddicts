#-*-coding: utf-8 -*-
from datetime import datetime, timedelta
from django.shortcuts import render
from livres.models import Livre
from commun.views import compteur

def nouveautes(request):
    compte = compteur()
    date_derniere_modif = datetime.now() - timedelta(14, 0, 0)
    livres = Livre.objects.filter(allowed=True).filter(date_modification__gte=date_derniere_modif.date()).order_by('-date_modification')
    return render(request, 'livres/nouveautes.html', locals())
