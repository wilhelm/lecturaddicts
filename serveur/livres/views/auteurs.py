#-*-coding: utf-8 -*-
from django.shortcuts import render
from livres.models import Auteur
from commun.views import compteur
from django.http import Http404

def par_auteur(request):
    compte = compteur()
    auteurs = Auteur.objects.raw('SELECT id, SUBSTR(nom, 1, 1) as premiere_lettre  FROM `livres_auteur` WHERE allowed=1 GROUP BY SUBSTR(nom, 1, 1)')
    return render(request, 'livres/par_auteur.html', locals())
    
def liste_auteurs(request):
    if request.method == "POST":
        auteurs = Auteur.objects.filter(allowed=True).filter(nom__startswith=request.POST.get("lettre", ""))
        return render(request, 'livres/liste_auteurs.html', locals())
    else:
        #auteurs = Auteur.objects.filter(allowed=True).filter(nom__startswith=request.GET.get("lettre", ""))
        #return render(request, 'livres/liste_auteurs.html', locals())
        raise Http404
