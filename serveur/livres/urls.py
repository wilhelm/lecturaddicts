# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from django.contrib import admin

urlpatterns = patterns('livres.views',
    url(r'^par_auteur$', 'par_auteur', name="par_auteur"),
    url(r'^par_titre$', 'par_titre', name="par_titre"),
    url(r'^nouveautes$', 'nouveautes', name="nouveautes"),
    url(r'^liste_auteurs$', 'liste_auteurs', name="liste_auteurs"),
)
