# -*- coding: utf-8 -*-
from django.db import models
#from livres import Livre
from rest_framework import viewsets, serializers, permissions
from lecturaddicts.perms import IsOwnerOrReadOnly

class Resume(models.Model):
    membre = models.ForeignKey('membres.Membre', related_name='membre_resume')
    livre = models.ForeignKey('livres.Livre', related_name="resumes")
    valeur = models.TextField()
    allowed = models.BooleanField(default=False)
    
    class Meta:
        app_label = 'membres'

class ResumeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Resume
        
    membre = serializers.PrimaryKeyRelatedField(read_only=True)
    livre = serializers.PrimaryKeyRelatedField(read_only=True)

    def create(self, validated_data):
        o = Resume.objects.create(**validated_data)
        o.membre = self.context.get_or_create('request').user
        return o

class ResumeViewSet(viewsets.ModelViewSet):
    queryset = Resume.objects.filter(allowed=True)
    serializer_class = ResumeSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly, )

    def create(self, request):
        s = ResumeSerializer(data=request.data, context={'request': request})
        if s.is_valid():
            s.save()
            return Response(s.data, status=status.HTTP_201_CREATED)
        else:
            return Response(s.errors, status=status.HTTP_400_BAD_REQUEST)
