# -*- coding: utf-8 -*-
from django.db import models
from rest_framework import viewsets, serializers, decorators, permissions
from rest_framework.response import Response
from lecturaddicts.perms import IsOwnerOrReadOnly

class Editeur(models.Model):
    nom = models.CharField(max_length=50)
    allowed = models.BooleanField(default=False)
    membre = models.ForeignKey('membres.Membre', related_name='membre_editeur')
    
    class Meta:
        app_label = 'livres'
        
class EditeurSerializer(serializers.ModelSerializer):
    class Meta:
        model = Editeur

    membre = serializers.PrimaryKeyRelatedField(read_only=True)

    def create(self, validated_data):
        o, c = Editeur.objects.get_or_create(**validated_data)
        if c:
            o.membre = self.context.get('request').user
            return o
        else: 
            return "La maison d'édition existe déjà."

class EditeurViewSet(viewsets.ModelViewSet):
    queryset = Editeur.objects.filter(allowed=True)
    serializer_class = EditeurSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly, )

    def create(self, request):
        s = EditeurSerializer(data=request.data, context={'request': request})
        if s.is_valid():
            s.save()
            return Response(s.data, status=status.HTTP_201_CREATED)
        else:
            return Response(s.errors, status=status.HTTP_400_BAD_REQUEST)
        
    @decorators.list_route(methods=['get'])
    def liste_admin(self, request):
        editeurs = Editeur.objects.filter(allowed=False).values()
        return Response(editeur, 200)
