# -*- coding: utf-8 -*-
from django.db import models
from rest_framework import viewsets, serializers, decorators, permissions
from rest_framework.response import Response
from django.core import serializers as s
from lecturaddicts.perms import IsOwnerOrReadOnly

class Auteur(models.Model):
    prenom = models.CharField(max_length=50, blank=True, null=True)
    nom = models.CharField(max_length=50)
    allowed = models.BooleanField(default=False)
    membre = models.ForeignKey('membres.Membre', related_name='membre_auteur')
    
    class Meta:
        app_label = 'livres'

class AuteurSerializer(serializers.ModelSerializer):
    class Meta:
        model = Auteur

    membre = serializers.PrimaryKeyRelatedField(read_only=True)

    def create(self, validated_data):
        o, c = Auteur.objects.get_or_create(**validated_data)
        if c:
            o.membre = self.context.get('request').user
            return o
        else:
            return "L'auteur existe déjà."

class AuteurViewSet(viewsets.ModelViewSet):
    queryset = Auteur.objects.filter(allowed=True)
    serializer_class = AuteurSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly, )

    def create(self, request):
        s = AuteurSerializer(data=request.data, context={'request': request})
        if s.is_valid():
            s.save()
            return Response(s.data, status=status.HTTP_201_CREATED)
        else:
            return Response(s.errors, status=status.HTTP_400_BAD_REQUEST)

    @decorators.list_route(methods=['get'])
    def lettres(self, request):
        lettres = Auteur.objects.raw('SELECT id, SUBSTR(nom, 1, 1) as premiere_lettre  FROM `livres_auteur` WHERE allowed=1 GROUP BY SUBSTR(nom, 1, 1)')
        liste = []
        for auteur in lettres:
            liste.append(auteur.premiere_lettre)
            print auteur.premiere_lettre

        return Response(liste, 200)

    @decorators.list_route(methods=['get'])
    def liste(self, request):
        auteurs = Auteur.objects.filter(allowed=1).filter(nom__startswith=request.GET.get("lettre", "")).order_by('nom').values('id', 'nom', 'prenom')
        return Response(auteurs, 200)
        
    @decorators.list_route(methods=['get'])
    def liste_admin(self, request):
        auteurs = Auteur.objects.filter(allowed=False).order_by('nom').values('id', 'nom', 'prenom', 'livre')
        return Response(auteurs, 200)
