# -*- coding: utf-8 -*-
from django.db import models
from django.db.models import Q, Case, Value, When, F, Count
from django.core.paginator import PageNotAnInteger, EmptyPage
from auteurs import Auteur, AuteurSerializer
from couvertures import CouvertureSerializer
from resumes import ResumeSerializer
from rest_framework import viewsets, serializers, decorators, permissions
from rest_framework.response import Response
from lecturaddicts.perms import IsOwnerOrReadOnly
from datetime import datetime, timedelta

class Livre(models.Model):
    titre_livre = models.CharField(max_length=100)
    titre_serie = models.CharField(max_length=100, blank=True, null=True)
    auteurs = models.ManyToManyField(Auteur)
    allowed = models.BooleanField(default=False)
    date_modification = models.DateTimeField(auto_now=True)
    membre = models.ForeignKey('membres.Membre', related_name='membre_livre')
    numero_tome = models.FloatField(null=True, blank=True)
    
    class Meta:
        app_label = 'livres'

class LivreSerializer(serializers.ModelSerializer):
    class Meta:
        model = Livre
        exclude = None
    
    auteurs = AuteurSerializer(read_only=True, many=True) #TODO : passer en False et débuger
    couvertures = CouvertureSerializer(read_only=True, many=True) #TODO : passer en False et débuger
    resumes = ResumeSerializer(read_only=True, many=True) #TODO : passer en False et débuger
    membre = serializers.PrimaryKeyRelatedField(read_only=True)

    def create(self, validated_data):
        o, c = Livre.objects.get_or_create(**validated_data)
        if c:
            o.membre = self.context.get('request').user
            return o
        else:
            return "Le livre existe déjà."

class LivreViewSet(viewsets.ModelViewSet):
    queryset = Livre.objects.filter(allowed=True)
    serializer_class = LivreSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly, )

    def create(self, request):
        s = LivreSerializer(data=request.data, context={'request': request})
        if s.is_valid():
            s.save()
            return Response(s.data, status=status.HTTP_201_CREATED)
        else:
            return Response(s.errors, status=status.HTTP_400_BAD_REQUEST)

    @decorators.list_route(methods=['get'])
    def lettres(self, request):
        lettres = Livre.objects.raw('SELECT id, SUBSTR(IF(NOT(titre_serie=""), titre_serie, titre_livre), 1, 1) as premiere_lettre  FROM `livres_livre` WHERE allowed=1 GROUP BY premiere_lettre')
        liste = []
        for livre in lettres:
            liste.append(livre.premiere_lettre)

        return Response(liste, 200)

    @decorators.list_route(methods=['get'])
    def liste(self, request):
        livres = Livre.objects.raw('SELECT * FROM `livres_livre` INNER JOIN (SELECT MIN(numero_tome) minNbTome, IF(NOT(titre_serie=""), titre_serie, titre_livre) as titre, COUNT(id) as nb_tomes FROM livres_livre WHERE allowed=1 GROUP BY titre) as l ON l.titre = IF(NOT(titre_serie=""), titre_serie, titre_livre) AND minNbTome = numero_tome WHERE allowed=1 GROUP BY titre HAVING SUBSTR(titre, 1, 1) = %s ORDER BY titre', [request.GET.get('lettre')])
        serializer = LivreSerializer(livres, many=True)
        for donnees, livre in zip(serializer.data, livres):
            donnees.update({'titre':livre.titre, 'nb_tomes': livre.nb_tomes})

        return Response(serializer.data, 200)

    @decorators.list_route(methods=['get'])
    def liste_admin(self, request):
        try:
            page = int(request.GET.get('page'))
        except ValueError:
            page = 0
        except TypeError:
            page = 0

        page *= 10
        livres = Livre.objects.raw('SELECT *, IF(NOT(titre_serie=""), titre_serie, titre_livre) as titre, COUNT(*) as nb_tomes FROM `livres_livre` WHERE allowed=0 GROUP BY IF(NOT(titre_serie=""), titre_serie, titre_livre) ORDER BY titre LIMIT %s, %s', [page, page+10])
        serializer = LivreSerializer(livres, many=True)
        for donnees, livre in zip(serializer.data, livres):
            donnees.update({'titre':livre.titre})

        return Response(serializer.data, 200)

    @decorators.list_route(methods=['get'])
    def liste_auteur(self, request):
        livres = Livre.objects.raw('SELECT * FROM livres_livre INNER JOIN (SELECT MIN(numero_tome) minNbTome, IF(NOT(titre_serie=""), titre_serie, titre_livre) as titre, COUNT(id) as nb_tomes FROM livres_livre WHERE allowed=1 GROUP BY titre) as l ON l.titre = IF(NOT(titre_serie=""), titre_serie, titre_livre) AND minNbTome = numero_tome JOIN livres_livre_auteurs ON livre_id=livres_livre.id WHERE allowed=1 AND auteur_id=%s GROUP BY titre ORDER BY titre', [request.GET.get('auteur')])
        serializer = LivreSerializer(livres, many=True)
        for donnees, livre in zip(serializer.data, livres):
            donnees.update({'titre': livre.titre, 'nb_tomes': livre.nb_tomes})

        return Response(serializer.data, 200)

    @decorators.list_route(methods=['get'])
    def liste_serie(self, request):
        if(request.GET.get("serie")):
            livres = Livre.objects.filter(titre_serie=request.GET.get("serie")).filter(allowed=True).order_by('numero_tome')
        else:
            livre = Livre.objects.get(pk=request.GET.get("serie_id"))
            livres = Livre.objects.filter(titre_serie=livre.titre_serie).filter(allowed=True).order_by('numero_tome')
        
        serializer = LivreSerializer(livres, many=True)
        return Response(serializer.data, 200)

    @decorators.list_route(methods=['get'])
    def liste_themes(self, request):
        livres = Livre.objects.raw('SELECT * FROM livres_livre INNER JOIN (SELECT MIN(numero_tome) minNbTome, IF(NOT(titre_serie=""), titre_serie, titre_livre) as titre, COUNT(id) as nb_tomes FROM livres_livre WHERE allowed=1 GROUP BY titre) as l ON l.titre = IF(NOT(titre_serie=""), titre_serie, titre_livre) AND minNbTome = numero_tome JOIN membres_theme ON livre_id=livres_livre.id WHERE allowed=1 AND valeur_id=%s GROUP BY titre ORDER BY titre', [request.GET.get('theme')])
        serializer = LivreSerializer(livres, many=True)
        for donnees, livre in zip(serializer.data, livres):
            donnees.update({'titre': livre.titre, 'nb_tomes': livre.nb_tomes})

        return Response(serializer.data, 200)

    @decorators.list_route(methods=['get'])
    def liste_notes(self, request):
        note = float(request.GET.get('note'))
        print note
        livres = Livre.objects.raw('SELECT *, AVG(valeur) as note_livre, COUNT(membres_note.id) as nb_votants FROM livres_livre JOIN membres_note ON livre_id=livres_livre.id WHERE allowed=1 GROUP BY livre_id HAVING note_livre > %s AND note_livre <= %s ORDER BY note_livre DESC, nb_votants DESC, titre_livre', [note-0.5, note])
        serializer = LivreSerializer(livres, many=True)
        for donnees, livre in zip(serializer.data, livres):
            donnees.update({'note': livre.note_livre})

        return Response(serializer.data, 200)

    @decorators.list_route(methods=['get'])
    def liste_nouveautes(self, request):
        livres = Livre.objects.filter(allowed=True).filter(date_modification__gte=(datetime.now() - timedelta(days=31)).date())
        serializer = LivreSerializer(livres, many=True)

        return Response(serializer.data, 200)
