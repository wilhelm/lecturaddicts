# -*- coding: utf-8 -*-
from django.db import models
from datetime import date
#from livres import Livre
from editeurs import Editeur
from rest_framework import viewsets, serializers, decorators, permissions
from rest_framework.response import Response
from lecturaddicts.perms import IsOwnerOrReadOnly

#class CouvertureManager(models.Manager):
#    def get_queryset(self):
#        return super(CouvertureManager, self).get_queryset().select_related('livre', 'editeur')

class Couverture(models.Model):
    livre = models.ForeignKey('livres.Livre', related_name='couvertures')
    editeur = models.ForeignKey(Editeur)
    date_publication = models.DateField(default=date(1970, 1, 1))
    allowed = models.BooleanField(default=False)
    membre = models.ForeignKey('membres.Membre', related_name='membre_couverture')
    couverture = models.ImageField(upload_to="couvertures/")
    miniature = models.ImageField(upload_to="miniatures/")
    
    class Meta:
        app_label = 'livres'

    def save(self, *args, **kwargs):
        from PIL import Image as im
        from cStringIO import StringIO
        from django.core.files.uploadedfile import SimpleUploadedFile
        
        def create_thumb(image, size):
            """Returns the image resized to fit inside a box of the given size"""
            image.thumbnail(size, im.ANTIALIAS)
            temp = StringIO()
            image.save(temp, 'png')
            temp.seek(0)
            return SimpleUploadedFile('temp', temp.read(), content_type='image/png')

        def has_changed(instance, field, manager='objects'):
            """Returns true if a field has changed in a model
        	May be used in a model.save() method.
        	"""
            if not instance.pk:
                return True
            manager = getattr(instance.__class__, manager)
            old = getattr(manager.get(pk=instance.pk), field)
            return not getattr(instance, field) == old

        if has_changed(self, 'couverture'):
            # on va convertir l'image en jpg
            filename = path.splitext(path.split(self.couverture.name)[-1])[0]
            filename = "%s.jpg" % filename

            image = im.open(self.couverture.file)

            if image.mode not in ('L', 'RGB'):
                image = image.convert('RGB')

            #le thumbnail
            self.miniature.save(
                    filename,
                    create_thumb(image, (100, 300)),
                    save=False)
            super(Image,self).save()
        else: 
            super(Image,self).save()
        
class CouvertureSerializer(serializers.ModelSerializer):
    class Meta:
        model = Couverture
        fields = ('livre', 'editeur', 'membre', 'date_publication', 'allowed', 'couverture_url', 'miniature_url')
    
    livre = serializers.PrimaryKeyRelatedField(read_only=True) #TODO : passer en False et débuger
    editeur = serializers.PrimaryKeyRelatedField(read_only=True) #TODO : passer en False et débuger
    membre = serializers.PrimaryKeyRelatedField(read_only=True)
    couverture_url = serializers.SerializerMethodField('get_couverture2_url')
    miniature_url = serializers.SerializerMethodField('get_image_url')

    def get_couverture2_url(self, obj):
        return obj.couverture.url

    def get_image_url(self, obj):
        if(not hasattr(obj.miniature, 'url')):
            return obj.couverture.url
        return obj.miniature.url

    def create(self, validated_data):
        o = Couverture.objects.create(**validated_data)
        o.membre = self.context.get('request').user
        return o

class CouvertureViewSet(viewsets.ModelViewSet):
    queryset = Couverture.objects.filter(allowed=True)
    serializer_class = CouvertureSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly, )

    def create(self, request):
        s = CouvertureSerializer(data=request.data, context={'request': request})
        if s.is_valid():
            s.save()
            return Response(s.data, status=status.HTTP_201_CREATED)
        else:
            return Response(s.errors, status=status.HTTP_400_BAD_REQUEST)

    @decorators.list_route(methods=['get'])
    def liste_admin(self, request):
        couvertures = Couverture.objects.filter(allowed=False).values()
        return Response(couvertures, 200)
