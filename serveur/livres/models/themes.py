# -*- coding: utf-8 -*-
from django.db import models
from django.db.models import Count
from rest_framework import viewsets, serializers, decorators, permissions
from membres.models import ThemeSerializer
from rest_framework.response import Response
from lecturaddicts.perms import IsOwnerOrReadOnly

class ThemePropose(models.Model):
    nom = models.CharField(max_length=25)
    description = models.CharField(max_length=100)
    allowed = models.BooleanField(default=False, blank=True)
    membre = models.ForeignKey('membres.Membre', related_name='membre_theme_propose')

    class Meta:
        app_label = 'livres'

class ThemeProposeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ThemePropose

    membre = serializers.PrimaryKeyRelatedField(read_only=True) #TODO : passer en False et débuger
    #c = serializers.IntegerField()

    def create(self, validated_data):
        o, c = ThemePropose.objects.get_or_create(**validated_data)
        if c:
            o.membre = self.context.get('request').user
            return o
        else:
            return "Le thème existe déjà."

class ThemeProposeViewSet(viewsets.ModelViewSet):
    queryset = ThemePropose.objects.filter(allowed=True)
    serializer_class = ThemeProposeSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly, )

    def create(self, request):
        s = ThemeProposeSerializer(data=request.data, context={'request': request})
        if s.is_valid():
            s.save()
            return Response(s.data, status=status.HTTP_201_CREATED)
        else:
            return Response(s.errors, status=status.HTTP_400_BAD_REQUEST)
        
    @decorators.list_route(methods=['get'])
    def liste_admin(self, request):
        themes = ThemePropose.objects.filter(allowed=False).values('id', 'nom', 'description', 'membre')
        return Response(themes, 200)

    @decorators.list_route(methods=['get'])
    def livre(self, request):
        print request.GET.get("livre")
        themes = ThemePropose.objects.raw('SELECT t1.*, livre_id, c FROM livres_themepropose as t1 LEFT JOIN (SELECT *, COUNT(valeur_id) as c FROM `membres_theme` WHERE livre_id=%s GROUP BY valeur_id ORDER BY c) as t ON valeur_id = t1.id WHERE allowed = 1 GROUP BY t1.id ORDER BY `c` DESC, t1.nom;', [request.GET.get('livre')])
        serializer = ThemeProposeSerializer(themes, many=True)
        print themes
        for donnees, theme in zip(serializer.data, themes):
            donnees.update({'c': theme.c})
        
        return Response(serializer.data, 200) #serializer.data, 200)
