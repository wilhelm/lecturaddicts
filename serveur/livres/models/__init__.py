from auteurs import Auteur, AuteurSerializer, AuteurViewSet
from editeurs import Editeur, EditeurSerializer, EditeurViewSet
from livres import Livre, LivreSerializer, LivreViewSet
from couvertures import Couverture, CouvertureSerializer, CouvertureViewSet
from themes import ThemePropose, ThemeProposeSerializer, ThemeProposeViewSet
from resumes import Resume, ResumeSerializer, ResumeViewSet

