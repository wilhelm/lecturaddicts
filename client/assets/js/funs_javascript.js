function afficher(elmt)
	{
	if (typeof elmt=="string")
		{
		elmt = document.getElementById(elmt);
		if (elmt)
			{
			if (elmt.style.display == "none")
				{
				elmt.style.display = "";
				}
			else
				{
				elmt.style.display = "none";
				};
			};
		};
	};
var alphabet1=new Array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
var alphabet2=new Array('0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
var sous_genre_recents=new Array();
sous_genre_recents['Am']='Histoire d\'amour';
sous_genre_recents['An']='Anges';
sous_genre_recents['V']='Vampires';
sous_genre_recents['MMA']='Monde moyen-âgeux';
sous_genre_recents['MPA']='Monde post-apocalyptique';
sous_genre_recents['LG']='Loups-garous';
sous_genre_recents['I']='Immortalité';
sous_genre_recents['M']='Magie';
sous_genre_recents['MP']='Monde parallèle';
sous_genre_recents['E']='Espionnage';
sous_genre_recents['Re']='Révolte';
sous_genre_recents['Po']='Policier';
sous_genre_recents['Fu']='Futuriste';
sous_genre_recents['Ic']='Inclassable';
var genre=new Array();
genre['Ac']='Livres récents';
genre['Vi']='Livres scolaires et anciens';
var sous_genre_classiques=new Array();
sous_genre_classiques['T']='Pièce de théâtre';
sous_genre_classiques['B']='Biographie';
sous_genre_classiques['F']='Fiction (roman)';
sous_genre_classiques['Ph']='Essai philosophique';
sous_genre_classiques['Ge']='Recueil de poèmes';
var themes=new Array();
themes['Am']='Histoire d\'amour';
themes['An']='Anges';
themes['V']='Vampires';
themes['MMA']='Monde moyen-âgeux';
themes['MPA']='Monde post-apocalyptique';
themes['LG']='Loups-garous';
themes['I']='Immortalité';
themes['M']='Magie';
themes['MP']='Monde parallèle';
themes['E']='Espionnage';
themes['Re']='Révolte';
themes['Po']='Policier';
themes['Fu']='Futuriste';
themes['Ic']='Inclassable';
themes['T']='Pièce de théâtre';
themes['B']='Biographie';
themes['Ph']='Essai philosophique';
themes['Ge']='Recueil de poèmes';
themes['C']='Classique';
function restart1(tab)
	{
	for(var i in tab)
		{
		if (document.getElementById(tab[i])!=null)
			{
			document.getElementById(tab[i]).style.display="none";
			};
		};
	};
function restart2(tab)
	{
	for(var i in tab)
		{
		if (document.getElementById('a'+tab[i])!=null)
			{
			document.getElementById('a'+tab[i]).style.display="none";
			};
		};
	};
function affichage1(elmt)
	{
	restart1(alphabet2);
	afficher(elmt);
	};
function affichage2(elmt)
	{
	restart2(alphabet2);
	afficher(elmt)
	};
function affichage3(elmt,n)
	{
	for(var i=0;i<=n;i=i+10)
		{
		document.getElementById('a'+i).style.display="none";
		};
	document.getElementById(elmt).style.display="";
	};
var auteur_precedent_sans_ajax="";
var serie_precedente_sans_ajax="";
function cacher_auteur_precedent(auteur)
	{
	if (auteur_precedent_sans_ajax!="")
		{
		document.getElementById(auteur_precedent_sans_ajax).style.display="none";
		}
	auteur_precedent_sans_ajax=auteur;
	};
function cacher_serie_precedente(serie)
	{
	if (serie_precedente_sans_ajax!="")
		{
		document.getElementById(serie_precedente_sans_ajax).style.display="none";
		}
	serie_precedente_sans_ajax=serie;
	};
function afficher_note(note)
	{
	restart_notes();
	afficher(note);
	};
function restart_notes()
	{
	for(var i=0.5;i<=10;i=i+0.5)
		{
		document.getElementById(i).style.display="none";
		};
	document.getElementById('nn').style.display="none";
	};
function afficher_onglets()
	{
	var liens=document.getElementById("liens");
	liens.innerHTML="<ul> <li> <a href=\"../base/pageaccueil.php\"> Accueil </a> </li> <li>	<a href=\"../base/nouveautes.php\"> Nouveautés </a> </li> <li> <a href=\"../base/parnote3.php\"> Par note </a> </li> <li> <a href=\"../base/partheme.php\"> Par thème </a> </li> <li> <a href=\"../base/partitre2.php\"> Par titre </a> </li> <li> <a href=\"../base/parauteur2.php\"> Par auteur </a>	</li> </ul>"+liens.innerHTML;
	};
function cacher_serie_genre()
	{
	var liste=document.getElementById("liste").getElementsByTagName('div');
	for (var i=0;i<liste.length;i++)
		{
		if (liste[i].className=='serie')
			{
			liste[i].style.display='none';
			}
		}
	};
function cacher_genres()
	{
	var tab=document.getElementById('T').getElementsByTagName('tr');
	for (var i=0;i<tab.length;i++)
		{
		if (!((i<5)&&(tab[i].className=='l')))
			{
			tab[i].style.display="none";
			};
		};
	afficher('plus');
	};
function afficher_modif_genres1()
	{
	var tab=document.getElementById('T').getElementsByTagName('tr');
	for (var i=0;i<tab.length;i++)
		{
		if (tab[i].className!='')
			{
			tab[i].style.display="";
			};
		};
	afficher('plus');
	afficher('moins');
	};
function afficher_modif_genres2()
	{
	var tab=document.getElementById('T').getElementsByTagName('tr');
	for (var i=0;i<tab.length;i++)
		{
		if (tab[i].className=='m')
			{
			tab[i].style.display="none";
			};
		};
	afficher('plus');
	afficher('moins');
	};
function cacher_ages()
	{
	var tab=document.getElementById('A').getElementsByTagName('tr');
	for (var i=0;i<tab.length;i++)
		{
		if (!((i<5)&&(tab[i].className=='a')))
			{
			tab[i].style.display='none';
			};
		};
	afficher('plusa');
	};
function afficher_modif_ages1()
	{
	var tab=document.getElementById('A').getElementsByTagName('tr');
	for (var i=0;i<tab.length;i++)
		{
		if (tab[i].className!='')
			{
			tab[i].style.display='';
			};
		};
	afficher('plusa');
	afficher('moinsa');
	};
function afficher_modif_ages2()
	{
	var tab=document.getElementById('A').getElementsByTagName('tr');
	for (var i=0;i<tab.length;i++)
		{
		if (tab[i].className=='a1')
			{
			tab[i].style.display='none';
			};
		};
	afficher('plusa');
	afficher('moinsa');
	};
function entree_valide_js(entree)
	{
	return entree.match('^[a-zA-Z0-9]{3,50}$');
	};
function mdp_valide_js(entree)
	{
	return ((entree_valide_js(entree))&&(entree.length<=50)&&(entree.length>=3));
	};
function confirme_mdp_js2(mdp,mdp2)
	{
	return ((mdp.search(new RegExp(mdp2))!=-1)&&(mdp2.search(new RegExp(mdp))!=-1));
	};
var message_info="Votre mot de passe, comme votre pseudo, ne doit pas contenir de caractère spécial. De plus, votre mot de passe doit comporter entre 3 et 50 caractères.";
function traitement_inscription()
	{
	var mdp=document.getElementById('mdp').value;
	var mdp2=document.getElementById('mdp2').value;
	var pseudo=document.getElementById('pseudo').value;
	if (!entree_valide_js(pseudo))
		{
		alert('Pseudo invalide.');
		alert(message_info);
		return false;
		}
	else
		{
		if (!entree_valide_js(mdp))
			{
			alert('Mot de passe invalide.');
			alert(message_info);
			return false;
			}
		else
			{
			if (!(mdp==mdp2))
				{
				alert('Le mot de passe et sa confirmation sont différents');
				return false;
				}
			};
		};
	};
function traitement_connexion()
	{
	var mdp=document.getElementById('mdp1').value;
	var pseudo=document.getElementById('pseudo1').value;
	if (!entree_valide_js(pseudo))
		{
		alert('Pseudo invalide.');
		alert(message_info);
		return false;
		}
	else
		{
		if (!entree_valide_js(mdp))
			{
			alert('Mot de passe invalide.');
			alert(message_info);
			return false;
			}
		};
	};
function simuler_clic_js()
	{
	if (document.getElementById('js'))
		{
		document.getElementById('js').click();
		}
	};
