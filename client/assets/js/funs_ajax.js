var xhr = null;
/*var i=-1;
var z=0;
var valeur_precedente='';
var valeur_precedente_auteur='';
var valeur_precedente_edition='';
var auteur_precedent="";
var serie_precedente="";
var note_precedente="";*/
function creer_element_xhr()  {
    if (window.XMLHttpRequest || window.ActiveXObject)  {
        if (window.ActiveXObject)  {
            try  {
                xhr = new ActiveXObject("Msxml2.XMLHTTP");
            } 
            catch(e)  {
                xhr = new ActiveXObject("Microsoft.XMLHTTP");
            }
        } 
        else  {
            xhr = new XMLHttpRequest();
        }
    }
    else  {
        alert("Votre navigateur ne supporte pas l'objet XMLHTTPRequest...");
        return null;
    };
    return xhr;
};

function envoi_donnees(pagetraitement,donnees,callback) {
    xhr=annule_requete_precedente(xhr);
    xhr=null;
    xhr=creer_element_xhr();
    xhr.onreadystatechange=function() {
        if ((xhr.readyState==4)&&((xhr.status==0)||(xhr.status==200))) {
            callback(xhr.responseText);
        }
    };
    xhr.open("POST",pagetraitement, true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.send(donnees);
};

function annule_requete_precedente() {
    if ((xhr!=null)&&(xhr.readyState!=0)) {
        try {
            xhr.abort();
        }
        catch(err) {
            alert(err.message);
        };
    }
    return xhr;
};

/*function afficher_liste_membres(value,page) {// fonction chapeau qui affichera la liste...
    if (document.getElementById('z1')) {
        var liste=document.getElementById('z1');
        liste.parentNode.removeChild(liste);
    }
    var donnees=encodeURIComponent(value);
    donnees='valeur='+donnees;
    envoi_donnees(page, donnees, creer_liste_membres); 
};

function creer_liste_membres(valeur) {
    var donnees=document.createElement('div');
    donnees.id='z1';
    var a=valeur.lastIndexOf(' ');
    z=valeur.substring(a+1);
    donnees.innerHTML=valeur.substring(0,a);
    document.getElementById("membre").appendChild(donnees);
};

function selectionner_membre(elmt) {
    elmt.className="selected";
};

function deselectionner_membre(elmt) {
    elmt.className="";
};

function valider_membre(elmt) {
    var data=elmt.innerHTML;
    document.getElementById('destinataire').value=data;
};

function gestion_clavier(e,valeur) {
    e=e||window.event;
    if (valeur!=valeur_precedente) {
        i=-1;
        valeur_precedente=valeur;
        afficher_liste_membres(valeur,'../fonctions/traitement_saisie_automatique_membres.php');
    }
    else {
        if (i>-1) {
            var b=true; // b indique si la sélection est déjà en-dehors du champ texte
        }
        if (i<z) {
            var c=true; // c indique si la sélection est dans les limites de la liste de membres
        }
        switch (e.keyCode) {
            case 38 : 
                if (b) {
                    if (c) {
                        // si un des éléments de la liste est sélectionné
                        deselectionner_membre(document.getElementById('b'+i));
                    }
                    i=i-1;
                }
                break;
            case 40 : 
                if (c) {
                    if (b) {
                        deselectionner_membre(document.getElementById('b'+i));
                    }
                    i=i+1;
                }
                break;
            case 13 : 
                if ((b)&&(c)) {
                    var k=1;
                }
                break;
            default : ;
        }
        if ((i>(-1))&&(i<z)) {
            selectionner_membre(document.getElementById('b'+i));
            if ((i<z-3)) {
                aligner_selection(-i);
            }
        }
        if (k==1) {
            valider_membre(document.getElementById('b'+i));
        }
    };
};

function aligner_selection(a) {
    for(var k=0;k<z;k++) {
        var t=a*21;
        document.getElementById('b'+k).style.position="relative";
        document.getElementById('b'+k).style.top=t+"px";
    };
}
function gestion_entree_souris(elmt) {
    deselectionner_membre(document.getElementById('b'+i));
    var tab=elmt.id.split('b');
    i=tab[1];
    selectionner_membre(elmt);
};

function gestion_sortie_souris(elmt) {
    deselectionner_membre(elmt);
    i=elmt.id.split('b')[1];
};

function supprimer_liste_membres() {
    if (document.getElementById('z1')) {
        var liste=document.getElementById('z1');
        liste.parentNode.removeChild(liste);
    }
};*/

function afficher_auteur_ajax(lettre) {
    if (document.getElementById('liste')!=null) {
        var liste=document.getElementById('liste');
        liste.parentNode.removeChild(liste);
    };
    envoi_donnees('liste_auteurs','lettre='+lettre, creer_liste);
};

function creer_liste(valeur) {
    var donnees=document.createElement('div');
    donnees.innerHTML=valeur;
    donnees.id='liste';
    document.getElementById('corps').appendChild(donnees);
};

/*function afficher_titre_ajax(lettre) {
    if (document.getElementById('liste')!=null) {
        var liste=document.getElementById('liste');
        liste.parentNode.removeChild(liste);
    };
    lettre=lettre.replace(/\'/,/'/);
    envoi_donnees('../listes/liste_livres_lettre_titre2.php','lettre='+lettre, creer_liste);
};

function afficher_genre_ajax(lettre) {
    if (document.getElementById('liste')!=null) {
        var liste=document.getElementById('liste');
        liste.parentNode.removeChild(liste);
    };

    function creation(valeur) {
        creer_genre(lettre,valeur);
    };

    envoi_donnees('../listes/liste_genre2.php','lettre='+lettre, creation);
};

function creer_genre(lettre,valeur) {
    var donnees=document.createElement('div');
    donnees.innerHTML=valeur;
    donnees.id='liste';
    document.getElementById(lettre).appendChild(donnees);
};

function afficher_liste_auteur(auteur) {
    if ((auteur_precedent!="")&&(document.getElementById(auteur_precedent)!=null)) {
        document.getElementById(auteur_precedent).innerHTML="";
    };
    auteur_precedent=remplace_pour_id(auteur);
    envoi_donnees('../listes/liste_livres2.php','auteur='+auteur, creer_liste_auteur);
};

function creer_liste_auteur(value) {
    var element=document.getElementById(auteur_precedent);
    element.innerHTML=value;
};

function remplace_pour_id(id) {
    id=id.replace(/ /g,'_');
    id=id.replace(/-/g,'_');
    id=id.replace(/'/g,'_');
    id=id.replace(/!/g,'_');
    return id;
};

function afficher_serie(serie) {
    if ((serie_precedente!="")&&(document.getElementById(serie_precedente))) {
        document.getElementById(serie_precedente).innerHTML="";
    };
    serie_precedente=remplace_pour_id(serie);
    envoi_donnees('../listes/liste_serie2.php','serie='+serie, creer_serie);
};

function creer_serie(value) {
    var element=document.getElementById(serie_precedente);
    element.innerHTML=value;
};

function afficher_note_ajax(note) {
    if ((note_precedente!="")&&(document.getElementById(note_precedente))) {
        document.getElementById(note_precedente).innerHTML="";
    };
    note_precedente=note;
    envoi_donnees('../listes/liste_livres_note2.php','note='+note, creer_note);
};

function creer_note(value) {
    var element=document.getElementById(note_precedente);
    element.innerHTML=value;
};

function creer_cookie_js() {
    envoi_donnees('../structure/creation_cookie_js.php','',fonction_js);
};

function fonction_js() {
};

function voter_themes(ID) {
    var ID_fiche=document.getElementsByClassName('ID_fiche')[0].value;
    var G=document.getElementById(ID).getElementsByClassName('G')[0].value;
    G=encodeURIComponent(G);
    ID_fiche=encodeURIComponent(ID_fiche);
    function voter(x) {
        if (x=='a') {
            var v=document.getElementById(ID).getElementsByClassName('bouton_genre')[0].value.split('+')[1];
            v=parseInt(v)+1;
            document.getElementById(ID).getElementsByClassName('bouton_genre')[0].value='+'+v;
        };
        return false;
    };
    envoi_donnees('../fonctions/voter_themes.php', 'ID_fiche='+ID_fiche+'&G='+G, voter);
};

function voter_ages(ID) {
    var ID_fiche=document.getElementsByClassName('ID_fiche')[0].value;
    var Age=document.getElementById(ID).getElementsByClassName('Age')[0].value;
    Age=encodeURIComponent(Age);
    ID_fiche=encodeURIComponent(ID_fiche);
    function voter(x) {
        if (x=='a') {
            var v=document.getElementById(ID).getElementsByClassName('bouton_genre')[0].value.split('+')[1];
            v=parseInt(v)+1;
            document.getElementById(ID).getElementsByClassName('bouton_genre')[0].value='+'+v;
        };
        return false;
    };
    envoi_donnees('../fonctions/voter_ages.php', 'ID_fiche='+ID_fiche+'&Age='+Age, voter);
};

function supprimer_fiche(id) {
    var ID=encodeURIComponent(id);
    envoi_donnees('../fonctions/supprimer_fiche2.php', 'ID='+ID, function(x) {if (x=='OK'){document.getElementById(id).parentNode.style.display="none"; return false;} else {alert('Loupé =)');}});
};

function traitement_recherche_membre() {
    var r=encodeURIComponent(document.getElementById('search_member').value);
    var t=encodeURIComponent(document.getElementById('type').value);
    var s=encodeURIComponent(document.getElementById('S').value);
    envoi_donnees('../fonctions/traitement_modifs_rapides.php','T='+t+'&R='+r+'&S='+s,creer_liste2);
    return false;
};

function traitement_recherche_membre_2() {
    var r=encodeURIComponent(document.getElementById('search_member').value);
    var t=encodeURIComponent(document.getElementById('type').value);
    var s=encodeURIComponent(document.getElementById('S').value);
    envoi_donnees('../fonctions/traitement_modifs_rapides_2.php','T='+t+'&R='+r+'&S='+s,creer_liste2);
    return false;
};

function traiter_modifs_rapides_2() {
    var tab=document.getElementsByTagName('input');
    var t=document.getElementById('T').value;
    var v=document.getElementById('v').value;
    alert(t);
    alert(v);
    if (document.getElementById('S').value=='s') {
        switch (t) {
            case 'A' :
                for(var k=0; k<tab.length;k++) {
                    if((tab[k].type=='checkbox')&&(tab[k].checked)) {alert('S,A'+tab[k].id);
                        var T=tab[k].id.split(',');
                        var ID_fiche=T[1];
                        A=encodeURIComponent(v);
                        ID_fiche=encodeURIComponent(ID_fiche);
                        if(T[0]=='S') {
                            envoi_donnees('../fonctions/voter_age_serie.php', 'ID_fiche='+ID_fiche+'&A='+A, afficher_OK);
                        }
                        else {
                            envoi_donnees('../fonctions/voter_ages.php', 'ID_fiche='+ID_fiche+'&A='+A, afficher_OK);
                        };
                    }
                };
                break;
            case 'T' : 
                for(var k=0; k<tab.length;k++) {
                    if((tab[k].type=='checkbox')&&(tab[k].checked)) {alert('S,T'+tab[k].id);

                        var T=tab[k].id.split(',');
                        var ID_fiche=T[1];
                        G=encodeURIComponent(v);
                        ID_fiche=encodeURIComponent(ID_fiche);
                        if(T[0]=='S') {
                            envoi_donnees('../fonctions/voter_themes_serie.php', 'ID_fiche='+ID_fiche+'&G='+G, afficher_OK);
                        }
                        else {
                            envoi_donnees('../fonctions/voter_themes.php', 'ID_fiche='+ID_fiche+'&G='+G, afficher_OK);
                        };
                    }
                }
                break;
            case 'N' : 
                for(var k=0; k<tab.length;k++) {
                    if((tab[k].type=='checkbox')&&(tab[k].checked)) {alert('S,N'+tab[k].id);
                        var T=tab[k].id.split(',');
                        var ID_fiche=T[1];
                        note=encodeURIComponent(v);
                        ID_fiche=encodeURIComponent(ID_fiche);
                        if (T[0]=='S') {
                            alert('S');
                            envoi_donnees('../fonctions/voter_note_serie.php', 'ID_fiche='+ID_fiche+'&note='+note, afficher_OK);
                        }
                        else {
                            envoi_donnees('../fonctions/voter_note.php', 'ID_fiche='+ID_fiche+'&note='+note, afficher_OK);
                        }
                    }
                }
                break;
            default : ;
        }
    }
    else  {
        switch (t) {
            case 'A' : 
                for(var k=0; k<tab.length;k++) {
                    if((tab[k].type=='checkbox')&&(tab[k].checked)) {alert('A'+tab[k].id);
                        alert(tab[k].value);
                        alert(tab[k].selected);
                        Age=encodeURIComponent(v);
                        ID_fiche=encodeURIComponent(tab[k].id);
                        envoi_donnees('../fonctions/voter_ages.php', 'ID_fiche='+ID_fiche+'&Age='+Age, afficher_OK);
                    }
                }
                break;
            case 'T' : 
                for(var k=0; k<tab.length;k++) {
                    if((tab[k].type=='checkbox')&&(tab[k].checked)) {alert('T'+tab[k].id);

                        G=encodeURIComponent(v);
                        ID_fiche=encodeURIComponent(tab[k].id);
                        envoi_donnees('../fonctions/voter_themes.php', 'ID_fiche='+ID_fiche+'&G='+G, afficher_OK);
                    }
                }
                break;
            case 'N' : 
                for(var k=0; k<tab.length;k++) {
                    if((tab[k].type=='checkbox')&&(tab[k].checked)) {alert('N'+tab[k].id);

                        note=encodeURIComponent(v);
                        ID_fiche=encodeURIComponent(tab[k].id);
                        envoi_donnees('../fonctions/voter_note.php', 'ID_fiche='+ID_fiche+'&note='+note, afficher_OK);
                        return false;
                    }
                }
                break;
            default : ;
        }
    }
    return false;
};

function creer_liste2(valeur) {
    document.getElementById('liste').innerHTML=valeur;
};

function traitement_age(t) {
    var ID_fiche=t;
    var Age=document.getElementById('a'+ID_fiche).value;
    Age=encodeURIComponent(Age);
    ID_fiche=encodeURIComponent(ID_fiche);
    envoi_donnees('../fonctions/voter_ages.php', 'ID_fiche='+ID_fiche+'&Age='+Age, afficher_OK);
    return false;
};

function traitement_themes(t) {
    var ID_fiche=t;
    var G=document.getElementById('t'+ID_fiche).value;
    G=encodeURIComponent(G);
    ID_fiche=encodeURIComponent(ID_fiche);
    envoi_donnees('../fonctions/voter_themes.php', 'ID_fiche='+ID_fiche+'&G='+G, afficher_OK);
    return false;
};

function traitement_note(t) {
    var ID_fiche=t;
    var note=document.getElementById(t+'note').value;
    note=encodeURIComponent(note);
    ID_fiche=encodeURIComponent(ID_fiche);
    envoi_donnees('../fonctions/voter_note.php', 'ID_fiche='+ID_fiche+'&note='+note, afficher_OK);
    return false;
};

function afficher_OK() {
    document.getElementById('confirm_modif').style.display="";
    setTimeout(afficher, 10000,'confirm_modif');
}
function traitement_note_serie(t) {
    var T=t.split(',');
    var ID_fiche=T[1];
    var note=document.getElementById(t+'note').value;
    note=encodeURIComponent(note);
    ID_fiche=encodeURIComponent(ID_fiche);
    if (T[0]=='S') {
        alert('S');
        envoi_donnees('../fonctions/voter_note_serie.php', 'ID_fiche='+ID_fiche+'&note='+note, afficher_OK);
    }
    else {
        envoi_donnees('../fonctions/voter_note.php', 'ID_fiche='+ID_fiche+'&note='+note, afficher_OK);
    }
    return false;
}
function traitement_themes_serie(t) {
    var T=t.split(',');
    var ID_fiche=T[1];
    var G=document.getElementById('t'+ID_fiche).value;
    G=encodeURIComponent(G);
    ID_fiche=encodeURIComponent(ID_fiche);
    if(T[0]=='S') {
        alert('S');
        envoi_donnees('../fonctions/voter_themes_serie.php', 'ID_fiche='+ID_fiche+'&G='+G, afficher_OK);
    }
    else {
        envoi_donnees('../fonctions/voter_themes.php', 'ID_fiche='+ID_fiche+'&G='+G, afficher_OK);
    };
    return false;
};

function traitement_age_serie(t) {
    var T=t.split(',');
    var ID_fiche=T[1];
    var A=document.getElementById('a'+t).value;
    A=encodeURIComponent(A);
    ID_fiche=encodeURIComponent(ID_fiche);
    if(T[0]=='S') {
        alert(A);
        alert(ID_fiche);
        envoi_donnees('../fonctions/voter_age_serie.php', 'ID_fiche='+ID_fiche+'&A='+A, afficher_OK);
    }
    else {
        envoi_donnees('../fonctions/voter_ages.php', 'ID_fiche='+ID_fiche+'&A='+A, afficher_OK);
    };
    return false;
};

function affichage(t) {
    document.getElementById('liste').innerHTML=t;
    return false;
};

function traitement_creation_fiche_rapide() {
    var U=document.getElementById('url').value;
    U=encodeURIComponent(U);
    envoi_donnees('../fonctions/apercu_fiche_rapide.php', 'U='+U, afficher_apercu);
    return false;
};

function afficher_apercu(a) {
    var t=a.split('\\');
    var e=document.getElementById('fiche').getElementsByClassName('a_remplir');
    e[0].src+=t[1];
    for(var i=1;i<6;i++) {
        e[i].innerText+=t[i+1];
    };
};

function recuperer_donnees() {
    var code=window.frames["myframe"].document;
    var p=code.getElementById('popinZoom').getElementsByTagName('li')[0].style.backgroundUrl;
    var l=code.getElementById('content').getElementsByTagName('span')[0].innerText;
    var a=code.getElementById('content').getElementsByTagName('a')[0].innerText;
    var r=code.getElementById('ficheResume').getElementsByClassName('avisEdContent')[0].innerHTML;
    var e=code.getElementById('content').getElementsByTagName('table')[0].getElementsByTagName('a')[0].innerText;
    var d=code.getElementById('content').getElementsByTagName('table')[0].getElementsByTagName('span')[1].innerText;
    document.getElementsByTagName('body').innerHTML='\\'+p+'\\'+l+'\\'+a+'\\'+r+'\\'+e+'\\'+d;
};

function afficher_liste_auteurs(value,page) {// fonction chapeau qui affichera la liste...
    if (document.getElementById('z1')) {
        var liste=document.getElementById('z1');
        liste.parentNode.removeChild(liste);
    }
    var donnees=encodeURIComponent(value);
    donnees='valeur='+donnees;
    envoi_donnees(page, donnees, creer_liste_auteurs); 
};

function creer_liste_auteurs(valeur) {
    var donnees=document.createElement('div');
    donnees.id='z1';
    var a=valeur.lastIndexOf(' ');
    z=valeur.substring(a+1);
    donnees.innerHTML=valeur.substring(0,a);
    document.getElementById("auteur").appendChild(donnees);
};

function selectionner_auteur(elmt) {
    elmt.className="selected";
};

function deselectionner_auteur(elmt) {
    elmt.className="";
};

function valider_auteur(elmt) {
    var data=elmt.innerHTML;
    document.getElementById('A').value=data;
};

function gestion_clavier_auteurs(e,valeur) {
    e=e||window.event;
    if (valeur!=valeur_precedente_auteur) {
        i=-1;
        valeur_precedente_auteur=valeur;
        afficher_liste_auteurs(valeur,'../fonctions/traitement_saisie_automatique_auteurs.php');
    }
    else {
        if (i>-1) {
            var b=true; //b indique si la sélection est déjà en-dehors du champ texte
        }
        if (i<z) {
            var c=true;// c indique si la sélection est dans les limites de la liste de membres 
        }
        switch (e.keyCode) {
            case 38 : 
                if (b) {
                    if (c) {
                        // si un des éléments de la liste est sélectionné
                        deselectionner_auteur(document.getElementById('b'+i));
                    }
                    i=i-1;
                }
                break;
            case 40 : 
                if (c) {
                    if (b) {
                        deselectionner_auteur(document.getElementById('b'+i));
                    }
                    i=i+1;
                }
                break;
            case 13 : 
                if ((b)&&(c)) {
                    var k=1;
                }
                break;
            default : ;
        }
        if ((i>(-1))&&(i<z)) {
            selectionner_auteur(document.getElementById('b'+i));
            if ((i<z-3)) {
                aligner_selection(-i);
            }
        }
        if (k==1) {
            valider_auteur(document.getElementById('b'+i));
        }
    };
};

function gestion_entree_souris_auteur(elmt) {
    deselectionner_auteur(document.getElementById('b'+i));
    var tab=elmt.id.split('b');
    i=tab[1];
    selectionner_auteur(elmt);
};

function gestion_sortie_souris_auteur(elmt) {
    deselectionner_auteur(elmt);
    i=elmt.id.split('b')[1];
};

function supprimer_liste_auteurs() {
    if (document.getElementById('z1')) {
        var liste=document.getElementById('z1');
        liste.parentNode.removeChild(liste);
    }
};

function afficher_liste_editions(value,page) {// fonction chapeau qui affichera la liste...
    if (document.getElementById('z1')) {
        var liste=document.getElementById('z1');
        liste.parentNode.removeChild(liste);
    }
    var donnees=encodeURIComponent(value);
    donnees='valeur='+donnees;
    envoi_donnees(page, donnees, creer_liste_editions); 
};

function creer_liste_editions(valeur) {
    var donnees=document.createElement('div');
    donnees.id='z1';
    var a=valeur.lastIndexOf(' ');
    z=valeur.substring(a+1);
    donnees.innerHTML=valeur.substring(0,a);
    document.getElementById("edition").appendChild(donnees);
};

function selectionner_edition(elmt) {
    elmt.className="selected";
};

function deselectionner_edition(elmt) {
    elmt.className="";
};

function valider_edition(elmt) {
    var data=elmt.innerHTML;
    document.getElementById('E').value=data;
};

function gestion_clavier_editions(e,valeur) {
    e=e||window.event;
    if (valeur!=valeur_precedente_edition) {
        i=-1;
        valeur_precedente_edition=valeur;
        afficher_liste_editions(valeur,'../fonctions/traitement_saisie_automatique_editions.php');
    }
    else {
        if (i>-1) {
            var b=true; // b indique si la sélection est déjà en-dehors du champ texte
        }
        if (i<z) {
            var c=true; // c indique si la sélection est dans les limites de la liste de membres
        }
        switch (e.keyCode) {
            case 38 : 
                if (b) {
                    if (c) {
                        // si un des éléments de la liste est sélectionné
                        deselectionner_edition(document.getElementById('b'+i));
                    }
                    i=i-1;
                }
                break;
            case 40 : 
                if (c) {
                    if (b) {
                        deselectionner_edition(document.getElementById('b'+i));
                    }
                    i=i+1;
                }
                break;
            case 13 : 
                if ((b)&&(c)) {
                    var k=1;
                }
                break;
            default : ;
        }
        if ((i>(-1))&&(i<z)) {
            selectionner_edition(document.getElementById('b'+i));
            if ((i<z-3)) {
                aligner_selection(-i);
            }
        }
        if (k==1) {
            valider_edition(document.getElementById('b'+i));
        }
    };
};

function gestion_entree_souris_edition(elmt) {
    deselectionner_edition(document.getElementById('b'+i));
    var tab=elmt.id.split('b');
    i=tab[1];
    selectionner_edition(elmt);
};

function gestion_sortie_souris_edition(elmt) {
    deselectionner_edition(elmt);
    i=elmt.id.split('b')[1];
};

function supprimer_liste_editions() {
    if (document.getElementById('z1')) {
        var liste=document.getElementById('z1');
        liste.parentNode.removeChild(liste);
    }
};

function traitement_fiche() {
    var a=fiche_existe(document.getElementById('L').value,document.getElementById('S').value,document.getElementById('A').value);
    var b=auteur_existe(document.getElementById('A').value);
    var c=edition_existe(document.getElementById('E').value);
    alert(b);
    alert(c);
    if ((!a)&&b&&c) {
    }
    else  {
        if (a) {
            return confirm('Une fiche ayant mêmes titre et auteur existe déjà. Êtes-vous sûr de vouloir créer cette fiche ?');
        }
        if (!b) {
            if (confirm('Cet auteur n\'existe pas. Êtes-vous sûr de vouloir le créer ?')) {
                creer_auteur(prompt('Nom de l\'auteur : '),prompt('Prénom de l\'auteur : '));
                alert('Auteur créé');
            }
        }
        if (!c) {
            if (confirm('Cette édition n\'existe pas. Êtes-vous sûr de vouloir la créer ?')) {
                creer_edition(prompt('Editions : '));
                alert('Edition créée');
            }
        }
    };
};

function fiche_existe(l,s,a) {
    var L=encodeURIComponent(l);
    var S=encodeURIComponent(s);
    var A=encodeURIComponent(a);
    return envoi_donnees('../fonctions/fiche_existe.php','L='+L+'&S='+S+'&A='+A, renvoyer_valeur);
};

function renvoyer_valeur(a) {
    if (a=='true') {
        return true;
    }
    else {
        return false;
    };
};

function auteur_existe(a) {
    var A=encodeURIComponent(a);
    return envoi_donnees('../fonctions/auteur_existe.php','A='+A, renvoyer_valeur);
};

function edition_existe(e) {
    var E=encodeURIComponent(e);
    return envoi_donnees('../fonctions/edition_existe.php','E='+E, renvoyer_valeur);
};

function creer_auteur(na,pa) {
    var NA=encodeURIComponent(na);
    var PA=encodeURIComponent(pa);
    envoi_donnees('../fonctions/creer_auteur.php','NA='+NA+'&PA='+PA, function(){});
};

function creer_edition(e) {
    var E=encodeURIComponent(e);
    envoi_donnees('../fonctions/creer_edition.php','E='+E, function(){});
};

*/
