'use strict';

angular.module('lecturaddicts')
    .controller('notes', ['$scope', '$http', 'APIURL', function ($scope, $http, APIURL) {
        //console.log("accueil");
        //console.log($scope.membre);
        $scope.range = function(num) {
            var range = [];
            for(var i=0.5;i<=num;i+=0.5) {
              range.push(i);
            }
            console.log(range);
            return range;
        }
        
        $scope.liste_livres = function(id) {
            console.log(id);
            $http.get(APIURL + '/livres/liste_notes/?note='+id).success(function(data) {
                $scope.liste_livres_note = data;
                console.log($scope.liste_livres_note);
            });
        };
    }])
;
