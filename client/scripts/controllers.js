'use strict';

angular.module('lecturaddicts')
    .controller('main', ['$scope', '$http', 'APIURL', 'auth.membre', '$state', '$location', '$anchorScroll', function ($scope, $http, APIURL, AuthMembre, $state, $location, $anchorScroll) {
        $scope.ancre = function(id) {
            // set the location.hash to the id of
            // the element you wish to scroll to.
            $location.hash(id);
            // call $anchorScroll()
            $anchorScroll();
        };

        $scope.membre = AuthMembre;
        //TODO: éviter la déconnexion à chaque rafraichissment de page
        //$scope.user = AuthMembre.login();
        $scope.logout = function() {
            $scope.membre.logout();
            $state.go('accueil');
        };

        $scope.liens_membres = {
            expand: function() {
                $scope.liens_expanded = !$scope.liens_expanded;
            }
        };
        $scope.liens_expanded = false;

        $scope.getMessages = function() {
        };
    }])
    .directive('listeAuteurs', function() {
        return {
            templateUrl: 'views/auteur.directive.html',
        };
    })
    .directive('listeSeries', function() {
        return {
            templateUrl: 'views/serie.directive.html',
        };
    })
    .directive('livre', function() {
        return {
            scope: {
                tome: '=',
            },
            templateUrl: 'views/livre.directive.html',
        };
    })
;
