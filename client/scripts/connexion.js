'use strict';

angular.module('lecturaddicts')
    .controller('connexion', ['$scope', 'auth.membre', '$http', 'APIURL', function ($scope, membre, $http, APIURL) {
        $scope.connexion = function (login) {
            $scope.loginError = false;
            $scope.inLogin = true;
            console.log(login);
            $scope.login={};
            membre.login(login).then(
                function(user) {
                    console.log(user);
                    $scope.login = {username: '', password: ''};
                    $scope.inLogin = false;
                }, function(error) {
                    console.log(error);
                    $scope.loginError = true;
                    $scope.login.password = '';
                    $scope.inLogin = false;
                }
            );
        };

        $scope.inscription = function(login) {
            if(login.password == login.password_confirm) {
                $http.post(APIURL + '/membres/', JSON.stringify(login)).success(function(data) {
                    $scope.connexion(login);
                    return data;
                });
            }
        };
    }])
;
