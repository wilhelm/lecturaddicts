'use strict';

angular.module('lecturaddicts')
    .controller('creation', ['$scope', 'auth.membre', '$state', '$http', function ($scope, membre, $http) {
        if(membre.isAuthenticated) {
            $scope.creation = function(livre) {
                $http.post(APIURL + '/livres/', {}).success();
            };
        }
        else {
            $state.go('accueil');
        }
    }])
;
