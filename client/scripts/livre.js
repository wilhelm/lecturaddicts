'use strict';

angular.module('lecturaddicts')
    .controller('livre', ['$scope', '$http', '$stateParams', 'APIURL', '$interval', function ($scope, $http, $stateParams, APIURL, $interval) {
        $scope.id = $stateParams.id;

        function incremente(compteur, nb_max) {
            if(compteur<nb_max-1) {
                compteur++;
            }
            else {
                compteur = 0;
            }
            return compteur;
        }

        $interval(function () {
            if($scope.livre != null) {
                $scope.c = incremente($scope.c, $scope.nb_couvertures);
            }
        }, 3000);

        $interval(function () {
            if($scope.livre != null) {
                console.log($scope.r);
                $scope.r = incremente($scope.r, $scope.nb_resumes);
            }
        }, 30000);

        /** récupration de toutes les données du livre **/
        $http.get(APIURL + '/livres/'+$scope.id+'/').success(function(data) {
            $scope.livre = data;
            $scope.nb_couvertures = $scope.livre.couvertures.length;
            $scope.nb_resumes = $scope.livre.resumes.length;
            $scope.c = 0;
            $scope.r = 0;
            console.log('livre');
            console.log(data);
        });

        /** affichage de tous les tomes de la serie **/
        $http.get(APIURL + '/livres/liste_serie/?serie_id='+$scope.id).success(function(data) {
            $scope.serie = data;
            console.log('serie');
            console.log(data);
        });

        /** themes **/
        /* deroulement */
        $scope.themes_expanded = false;
        $scope.themes_expand = function() {
            $scope.themes_expanded = !$scope.themes_expanded;
        };

        /* récupération */
        $http.get(APIURL + '/themes_proposes/livre/?livre='+$scope.id).success(function(data) {
            $scope.themes = data;
            console.log('themes');
            console.log(data);
        });

        /** ages **/
        /* deroulement */
        $scope.ages_expanded = false;
        $scope.ages_expand = function() {
            $scope.ages_expanded = !$scope.ages_expanded;
        };

        /* récupération */
        $http.get(APIURL + '/ages/livre/?livre='+$scope.id).success(function(data) {
            $scope.ages = data;
            console.log('ages');
            console.log(data);
        });

        /** notes **/
        /* récupération */
        $http.get(APIURL + '/notes/livre/?livre='+$scope.id).success(function(data) {
            $scope.note = data;
            console.log('notes');
            console.log(data);
        });

        if($scope.membre.isAuthenticated()) {
            console.log($scope.membre);
            $http.get(APIURL + '/notes/livre_membre/?livre='+$scope.id).success(function(data) {
                $scope.note_membre = data;
                console.log('note_membre');
                console.log(data);
            });
        }

        /* vote */
        $scope.vote_note = function(note_votee) {
            note_votee.livre = $scope.id;
            console.log($scope.membre);
            note_votee.membre = $scope.membre.membre.id;
            console.log(JSON.stringify(note_votee));
            $http.post(APIURL + '/notes/', JSON.stringify(note_votee)).success(function(data) {
                console.log(data);
                $scope.note_membre = data
                return data;
            });
        };

        /** commentaires **/
        /* enumeration de pages de commentaires */
        $scope.range = function(num) {
            var range = [];
            for(var i=1;i<=num;i++) {
              range.push(i);
            }
            return range;
        };

        /* récupération */
        $http.get(APIURL + '/commentaires/livre/?livre='+$scope.id).success(function(data) {
            $scope.commentaires = data.results;
            $scope.nb_commentaires = data.count;
            console.log('commentaires');
            console.log($scope.nb_commentaires);
            console.log(data);
        });

        /* changement de page */
        $scope.change_liste_commentaires = function(n) {
            $http.get(APIURL + '/commentaires/livre/?livre='+$scope.id+'&page='+n).success(function(data) {
                $scope.commentaires = data.results;
                console.log('commentaires');
                console.log($scope.nb_commentaires);
                console.log(data);
            });
        };

        /* ajout */
        $scope.poster_commentaire = function(commentaire) {
            console.log(commentaire);
            var comment={"valeur": commentaire};
            console.log(comment);
            comment.livre = $scope.id;
            console.log($scope.membre);
            comment.membre = $scope.membre.membre.id;
            console.log(JSON.stringify(comment));
            $http.post(APIURL + '/commentaires/', JSON.stringify(comment)).success(function(data) {
                console.log(data);
                $scope.commentaire = null;
                //TODO: supprimer le texte posté, et rafraîchir les commentaires
                return data;
            });
        };
    }])
;
