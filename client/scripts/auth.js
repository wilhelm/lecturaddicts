'use strict';

angular.module('lecturaddicts.auth', [
    'ngStorage',
])

/**
 * Gère la base de l'authentification
 * Ne pas utiliser directement ce service pour se connecter, utiliser plutôt auth.user
 */
// cannot inject $http directly because it would cause a conflict when registering AuthInterceptor
.factory('auth.service',
    ['$injector', '$localStorage', '$q', 'APIURL',
    function ($injector, $localStorage, $q, APIURL) {
        if ($localStorage.auth === undefined) {
            $localStorage.auth = {
                token: null
            };
        }
        return {
            login: function(credentials) {
                return $injector.get('$http').post(APIURL + '/api-token-auth/', credentials, {'headers':{'Content-Type':"application/json"}}).then(
                    function(response) {
                        $localStorage.auth.token = response.data.token;
                        return credentials;
                    },
                    function(response) {
                        $localStorage.auth.token = null;
                        return $q.reject(response);
                    });
            },
            logout: function() {
                $localStorage.auth.token = null;
            },
            isAuthenticated: function() {
                return $localStorage.auth.token !== null;
            },
            getToken: function() {
                return $localStorage.auth.token;
            }
        };
}])

.factory('auth.membre',
    ['auth.service', '$rootScope', '$q', '$timeout', '$location', '$state', '$stateParams', 'APIURL', '$http', 
    function (AuthService, $rootScope, $q, $timeout, $location, $state, $stateParams, APIURL, $http) {
        return {
            membre: null,
            /**
             * Essaie de se connecter avec les identifiants passés en paramètre,
             * et si cela réussi, alors récupère le compte de l'utilisateur
             */
            login: function(credentials) {
                var self = this;
                return AuthService.login(credentials).then(
                    function(membre) {
                        $state.go('accueil');
                        $http.get(APIURL + '/membres/me/').success(function(data) {
                            delete self.username;
                            delete self.password;
                            self.membre = data;
                            return data;
                        });
                    }, function(response) {
                        return $q.reject(response);
                    }
                );
            },

            /**
             * Déconnecte l'utilisateur et supprime ses données stockées
             */
            logout: function() {
                AuthService.logout();
                this.membre = null;
                $rootScope.$broadcast('auth.hasLoggedOut');
                $state.go('accueil');
            },
            isAuthenticated: function() {
                return this.membre != null;
            },
            isAdmin: function() {
                //console.log(this.membre);
                return this.membre.is_staff;
            }
        };
    }])

/**
 * Intercepte toutes les requêtes pour y ajouter le token de l'utilisateur connecté
 */
.factory('auth.interceptor', ['auth.service', '$q',
    function (AuthService, $q) {
        return {
            request: function(config) {
                config.headers = config.headers || {};
                if (AuthService.isAuthenticated() && !/\/off\//.test(config.url) && !/\/ooshop\//.test(config.url) && !/\/autoappro\//.test(config.url) && !/fr\.openfoodfacts\.org/.test(config.url)) {
                    config.headers.Authorization = 'JWT ' + AuthService.getToken();
                }

                // config.params = config.params || {};
                // // to improve: necessary for ui.bootstrap ; and the token is useless for static files
                // if (AuthService.isAuthenticated() && /^((http)|[^a-z])/.test(config.url)) {
                //     config.params["bearer"] = AuthService.getToken();
                // }
                //console.log(config);
                return config || $q.when(config);
            },
            response: function(response) {
                if (response.status === 401) {
                    AuthService.logout();
                    // TODO: Redirect user to login page.
                }
                return response || $q.when(response);
            },
            responseError: function(response) {
                console.log(response);
                if (response.status === 401) {
                    AuthService.logout();
                    // TODO: Redirect user to login page.
                }
                return $q.reject(response);
            }
        };
}]);
