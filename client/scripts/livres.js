'use strict';

angular.module('lecturaddicts')
    .controller('livres', ['$scope', '$http', 'APIURL', function ($scope, $http, APIURL) {
        //console.log("accueil");
        //console.log($scope.membre);
        $http.get(APIURL + '/livres/lettres/').success(function(data) {
            $scope.lettres = data;
            console.log(data);
        });
        $scope.liste_livres = function(lettre) {
            console.log(lettre);
            $scope.serie_selectionnee = "";
            $scope.tomes = null;
            $http.get(APIURL + '/livres/liste/?lettre='+lettre).success(function(data) {
                $scope.liste_livres_lettre = data;
                console.log($scope.liste_livres_lettre);
            });
        };
        $scope.liste_serie = function(serie) {
            if($scope.serie_selectionnee != serie) {
                $scope.serie_selectionnee = serie;
            }
            else {
                $scope.serie_selectionnee = "";
            }
            console.log("serie : "+serie);
            $http.get(APIURL + '/livres/liste_serie/?serie='+serie).success(function(data) {
                $scope.tomes = data;
                console.log($scope.tomes);
            });
        };
    }])
;
