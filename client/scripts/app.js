'use strict';

angular
    .module('lecturaddicts', [
        'ngResource',
        'ngSanitize',
        'ui.router',
        'API',
        'lecturaddicts.auth'
    ])
    .config(['$httpProvider', 'APIURLProvider',  function($httpProvider, APIURL) {
        APIURL.url = "http://127.0.0.1:8000";
        $httpProvider.interceptors.push('auth.interceptor');
    }])
    .config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('accueil', {
                url: "/",
                data: {
                    title: 'Lectur\'Addicts',
                    description: "Pour discuter des livres de vos auteurs préférés, et des autres aussi",
                    robots: 'index, follow',
                    css_additionnels: [
                        'accueil'
                    ]
                },
                templateUrl: 'views/accueil.html',
                controller: 'main'
                
            })
            .state('par_auteur', {
                url: "/par_auteur",
                data: {
                    title: 'Lectur\'Addicts : Auteurs',
                    description: "Pour discuter des livres de vos auteurs préférés, et des autres aussi",
                    robots: 'index, follow',
                    css_additionnels: [
                        'listes'
                    ]
                },
                templateUrl: 'views/auteurs.html',
                controller: 'auteurs'
            })
            .state('par_titre', {
                url: "/par_titre",
                data: {
                    title: 'Lectur\'Addicts : Livres',
                    description: "Pour discuter des livres de vos auteurs préférés, et des autres aussi",
                    robots: 'index, follow',
                    css_additionnels: [
                        'listes'
                    ]
                },
                templateUrl: 'views/livres.html',
                controller: 'livres'
            })
            .state('par_theme', {
                url: "/par_theme",
                data: {
                    title: 'Lectur\'Addicts : Themes',
                    description: "Pour discuter des livres de vos auteurs préférés, et des autres aussi",
                    robots: 'index, follow',
                    css_additionnels: [
                        'listes'
                    ]
                },
                templateUrl: 'views/themes.html',
                controller: 'themes'
            })
            .state('par_note', {
                url: "/par_note",
                data: {
                    title: 'Lectur\'Addicts : Notes',
                    description: "Pour discuter des livres de vos auteurs préférés, et des autres aussi",
                    robots: 'index, follow',
                    css_additionnels: [
                        'listes'
                    ]
                },
                templateUrl: 'views/notes.html',
                controller: 'notes'
            })
            .state('nouveautes', {
                url: "/nouveautes",
                data: {
                    title: 'Lectur\'Addicts : Nouveautés',
                    description: "Pour discuter des livres de vos auteurs préférés, et des autres aussi",
                    robots: 'index, follow',
                    css_additionnels: [
                        'listes'
                    ]
                },
                templateUrl: 'views/nouveautes.html',
                controller: 'nouveautes'
            })
            .state('livre', {
                url: "/livre/:id",
                templateUrl: 'views/livre.html',
                data: {
                    title: 'Lectur\'Addicts : Livre',
                    description: "Pour discuter des livres de vos auteurs préférés, et des autres aussi",
                    robots: 'index, follow',
                    css_additionnels: [
                        'fiche2'
                    ]
                },
                controller: 'livre'
            })
            .state('connexion', {
                url: "/connexion",
                templateUrl: 'views/connexion.html',
                data: {
                    title: 'Lectur\'Addicts : Connexion',
                    description: "Pour discuter des livres de vos auteurs préférés, et des autres aussi",
                    robots: 'index, follow',
                    css_additionnels: [
                        'connexion'
                    ]
                },
                controller: 'connexion'
            })
            .state('autoriser', {
                url: "/autoriser",
                templateUrl: 'views/autoriser.html',
                data: {
                    title: 'Lectur\'Addicts : Autoriser',
                    description: "Pour discuter des livres de vos auteurs préférés, et des autres aussi",
                    robots: 'index, follow',
                    css_additionnels: [
                    ]
                },
                controller: 'autoriser'
            })
            .state('creation', {
                url: "/creation",
                templateUrl: 'views/creation.html',
                data: {
                    title: 'Lectur\'Addicts : Création d\'une nouvelle fiche',
                    description: "Pour discuter des livres de vos auteurs préférés, et des autres aussi",
                    robots: 'index, follow',
                    css_additionnels: [
                        'creation'
                    ]
                },
                controller: 'creation'
            });
        $urlRouterProvider.otherwise('/');
    }])
    .filter('nl2br', ['$sce', function ($sce) {
        return function (text) {
            return text ? $sce.trustAsHtml('<p class="paragraphe">'+text.replace(/\n/g, '</p><p class="paragraphe">')+'</p>') : '';
        };
    }])
    .filter('br', ['$sce', function ($sce) {
        return function (text) {
            return text ? $sce.trustAsHtml(text.toString().replace(/<br \/>/g, '')) : '';
        };
    }])
    .run(['$rootScope', '$state', '$stateParams', function($rootScope, $state, $stateParams) {
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
        $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
            $rootScope.title = toState.data.title;
            $rootScope.description = toState.data.description;
            $rootScope.robots = toState.data.robots;
            $rootScope.css_additionnels = toState.data.css_additionnels;
        });
    }]);
