'use strict';

angular.module('lecturaddicts')
    .controller('nouveautes', ['$scope', '$http', 'APIURL', function ($scope, $http, APIURL) {        
        $http.get(APIURL + '/livres/liste_nouveautes/').success(function(data) {
            $scope.liste_livres_nouveautes = data;
            console.log($scope.liste_livres_nouveautes);
        });
    }])
;
