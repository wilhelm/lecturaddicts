'use strict';

angular.module('lecturaddicts')
    .controller('auteurs', ['$scope', '$http', 'APIURL', function ($scope, $http, APIURL) {
        //console.log("accueil");
        //console.log($scope.membre);
        $http.get(APIURL + '/auteurs/lettres/').success(function(data) {
            $scope.auteurs = data;
            //console.log(data);
        });
        $scope.liste_auteurs = function(lettre) {
            console.log(lettre);
            $scope.auteur_selectionne = "";
            $scope.livres = null;
            $http.get(APIURL + '/auteurs/liste/?lettre='+lettre).success(function(data) {
                $scope.liste_auteurs_lettre = data;
                //console.log($scope.liste_auteurs_lettre);
            });
        };
        $scope.liste_livres = function(id) {
            if($scope.auteur_selectionne != id) {
                //console.log(id);
                $http.get(APIURL + '/livres/liste_auteur/?auteur='+id).success(function(data) {
                    $scope.livres = data;
                    //console.log(data[0]);
                });
                $scope.auteur_selectionne = id;
            }
            else {
                $scope.auteur_selectionne = "";
            }
            $scope.tomes = null;
            $scope.serie_selectionnee = "";
        };
        $scope.liste_serie = function(serie) {
            if($scope.serie_selectionnee != serie) {
                $scope.serie_selectionnee = serie;
            }
            else {
                $scope.serie_selectionnee = "";
            }
            console.log("serie : "+serie);
            $http.get(APIURL + '/livres/liste_serie/?serie='+serie).success(function(data) {
                $scope.tomes = data;
                //console.log($scope.tomes);
            });
        };
    }])
;
