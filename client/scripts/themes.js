'use strict';

angular.module('lecturaddicts')
    .controller('themes', ['$scope', '$http', 'APIURL', function ($scope, $http, APIURL) {
        //console.log("accueil");
        //console.log($scope.membre);
        $http.get(APIURL + '/themes_proposes/').success(function(data) {
            $scope.themes = data.results;
            console.log($scope.themes);
            console.log($scope.themes[0]);
        });
        $scope.liste_livres = function(id) {
            console.log(id);
            $scope.serie_selectionnee = "";
            $scope.tomes = null;
            $http.get(APIURL + '/livres/liste_themes/?theme='+id).success(function(data) {
                $scope.liste_livres_theme = data;
                console.log($scope.liste_livres_theme);
            });
        };
        $scope.liste_serie = function(serie) {
            if($scope.serie_selectionnee != serie) {
                $scope.serie_selectionnee = serie;
            }
            else {
                $scope.serie_selectionnee = "";
            }
            console.log("serie : "+serie);
            $http.get(APIURL + '/livres/liste_serie/?serie='+serie).success(function(data) {
                $scope.tomes = data;
                console.log($scope.tomes);
            });
        };
    }])
;
